## Practical Programming and Numerical Methods

This repository contains the excercises and problems solved, in following the 
course "Practical Programming and Numerical Methods" (ppnm) in the spring of 2020 
at the Institute of Physics and Astronomy at Aarhus University, Denmark.

All contents are made in the C# programming language.

##### Problem folder structure
Each folder contains a **lib** folder containing the methods that can be called. 
The other folders contains Makefiles etc. showcasing the methods.

// Rasmus Burlund Fink
