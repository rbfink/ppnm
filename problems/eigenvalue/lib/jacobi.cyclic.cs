using static System.Math;

public static partial class MatrixManipulation
{
	public static vector JDCyclic(matrix A, matrix V, double acc=1e-6)
	{
		/* Error handling */
		if (A.size1!=A.size2 | V.size1!=V.size2)
		{throw new System.ArgumentException("The A or V matrix is not the right size or not square!");}
		if (A.equals(A.T)==false)
		{throw new System.ArgumentException("This method only accepts real symmetric matrices !");}

		System.IO.TextWriter stderr = System.Console.Error;

		V.set_identity();
		double c, s, t, App, Aqq, Apq, Vip, Viq;
		int dim = A.size1;
		int counter = 0;
		bool changed;
		vector eigvalvec = new vector(dim);
		
		do
		{
			changed = false;
			/* sweeping off-diagonal elements */
			for (int p=0; p<dim; p++)
			{		
				for (int q=p+1; q<dim; q++)
				{		
					App = A[p,p];
					Aqq = A[q,q];
					Apq = A[p,q];
	
					/* calculate c and s values for rotation */
					t = 0.5*Atan2(2*Apq, Aqq - App);
					c = Cos(t);
					s = Sin(t);

					/* now H is rotated (eq. 10 in notes) */
					A[p,p] = c*c*App - 2*s*c*Apq + s*s*Aqq;
					A[q,q] = s*s*App + 2*s*c*Apq + c*c*Aqq;
					A[p,q] = c*s*(App - Aqq) + (c*c - s*s)*Apq;
					
					if (App != A[p,p] || Aqq != A[q,q])
					{
						changed = true;

						eigvalvec[p] = A[p,p];
						eigvalvec[q] = A[q,q];
						
						for (int i=0; i<p; i++)
						{
							double Aip = A[i,p];
							double Aiq = A[i,q];
							A[i,p] = c*Aip - s*Aiq;
							A[i,q] = s*Aip + c*Aiq;
						}
						for (int i=p+1; i<q; i++)
						{
							double Api = A[p,i];
							double Aiq = A[i,q];
							A[p,i] = c*Api - s*Aiq;
							A[i,q] = s*Api + c*Aiq;
						}
						for (int i=q+1; i<dim; i++)
						{
							double Api = A[p,i];
							double Aqi = A[q,i];
							A[p,i] = c*Api - s*Aqi;
							A[q,i] = s*Api + c*Aqi;
						}
						for (int i=0; i<dim; i++)
						{
							Vip = V[i,p];
							Viq = V[i,q];
							V[i,p] = c*Vip - s*Viq;
							V[i,q] = s*Vip + c*Viq;
						}
					counter++;
					}// if
				}// for q
			}// for p
		}while (changed);
		
		stderr.WriteLine($" Cyclic {dim}x{dim}  matrix, Rotations = {counter}");

		return eigvalvec;
	}// JacobiDiagonalization
	
}// MatrixManipulation
