using static System.Math;

public static partial class MatrixManipulation
{
	public static vector JDege(matrix A, matrix V, int NrOfEigvals, double acc=1e-6)
	{
		/* Error handling */
		if (A.size1!=A.size2 | V.size1!=V.size2)
		{throw new System.ArgumentException("The A or V matrix is not the right size or not square!");}
		if (A.equals(A.T)==false)
		{throw new System.ArgumentException("This method only accepts real symmetric matrices !");}

		System.IO.TextWriter stderr = System.Console.Error;

		V.set_identity();
		double c, s, t, App, Aqq, Apq;
		int dim = A.size1;
		int counter = 0;
		bool changed;
		vector eigvalvec = new vector(NrOfEigvals);
		

		for (int p=0; p<NrOfEigvals; p++)
		{
			do
			{
				changed = false;
				/* sweeping off-diagonal elements */
				for (int q=p+1; q<dim; q++)
				{		
					App = A[p,p];
					Aqq = A[q,q];
					Apq = A[p,q];
	
					/* calculate c and s values for rotation */
					t = 0.5*Atan2(2*Apq, Aqq - App);
					c = Cos(t);
					s = Sin(t);

					/* now H is rotated (eq. 10 in notes) */
					double app = c*c*App - 2*s*c*Apq + s*s*Aqq;
					double aqq = s*s*App + 2*s*c*Apq + c*c*Aqq;
					
					if (app != App || aqq != Aqq)
					{
						changed = true;

						A[p,p] = app;
						A[q,q] = aqq;
						A[p,q] = 0.0;						
						for (int i=p+1; i<q; i++)
						{
							double Api = A[p,i];
							double Aiq = A[i,q];
							A[p,i] = c*Api - s*Aiq;
							A[i,q] = s*Api + c*Aiq;
						}
						for (int i=q+1; i<dim; i++)
						{
							double Api = A[p,i];
							double Aqi = A[q,i];
							A[p,i] = c*Api - s*Aqi;
							A[q,i] = c*Aqi + s*Api;
						}
					counter++;
					}// if
				}// for q
			}while (changed);
		}// for p

		for (int i=0; i<NrOfEigvals; i++)
		{
			eigvalvec[i] = A[i,i];
		}		
		stderr.WriteLine($" Rotations = {counter}");

		return eigvalvec;
	}// Jacobi_eigenvalue_by_eigenvalue
}// MatrixManipulation
