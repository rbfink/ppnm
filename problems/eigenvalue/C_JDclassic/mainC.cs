using System.Diagnostics;
using static System.Math;
using static System.Console;
using static matrixhelp;
using static MatrixManipulation;

static class main
{
	static int Main()
	{
		int randim = 20;
		matrix A = RandomSymMatrix(randim);
		matrix B = A.copy();
		matrix V = new matrix(randim,randim);
		vector raneig = JDClassic(B,V);
		matrix D = new matrix(A.size1,A.size1);

		for (int i=0; i<A.size1; i++)
		{
			D[i,i] = raneig[i];
		}

		WriteLine("\n --- Test of classic Jacobi algorithm --- \n");
		WriteLine($" Testing done with {randim}x{randim} random symmetric matrix ");
		WriteLine($" A = V*D*V^T --> {A.equals(V*D*V.transpose())}");
		WriteLine($" D = V^T*A*V --> {D.equals(V.transpose()*A*V)}");

		WriteLine("\n NOTE: number of rotations can be seen in log.tbc.txt \n");
		return 0;
	}// JacobiClassicTest
}
