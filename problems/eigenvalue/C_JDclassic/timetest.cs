using System.Diagnostics;
using static System.Math;
using static System.Console;
using static matrixhelp;
using static MatrixManipulation;

class main
{
	public static int Main()
	{
		Stopwatch sw = new System.Diagnostics.Stopwatch();
		var DataWriter = new System.IO.StreamWriter("timetestdata.tbc.txt");
		long classicTime, cyclicTime;
		for (int dim=50; dim<=500; dim+=50)
		{
			matrix Aclassic = RandomSymMatrix(dim,dim);
			matrix Vclassic = new matrix(dim,dim);
			matrix Acyclic  = Aclassic.copy();
			matrix Vcyclic  = Vclassic.copy();
			sw.Start();
			JDCyclic(Acyclic,Vcyclic);
			sw.Stop();
			cyclicTime = sw.ElapsedMilliseconds;
			sw.Reset();
			sw.Start();
			JDClassic(Aclassic,Vclassic);
			sw.Stop();
			classicTime = sw.ElapsedMilliseconds;
			sw.Reset();
			DataWriter.WriteLine("{0} {1} {2}", dim, cyclicTime, classicTime);
		}
		DataWriter.Close();
		return 0;
	}
}
