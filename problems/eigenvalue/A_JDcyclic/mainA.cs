using static System.Math;
using static System.Console;
using static matrixhelp;
using static MatrixManipulation;

static class main
{
	static int Main()
	{
		/* RANDOM MATRIX TEST ----------------------------------------------- */
		int randim = 20;
		matrix A = RandomSymMatrix(randim);
		matrix B = A.copy();
		matrix V = new matrix(randim,randim);
		vector raneig = JDCyclic(B,V);

		matrix D = new matrix(A.size1,A.size1);
		for (int i=0; i<A.size1; i++)
		{
			D[i,i] = raneig[i];
		}

		WriteLine("\n --- Test of cyclic Jacobi algorithm --- \n");
		WriteLine($" Testing done with {randim}x{randim} random symmetric matrix ");
		WriteLine($" A = V*D*V^T --> {A.equals(V*D*V.transpose())}");
		WriteLine($" D = V^T*A*V --> {D.equals(V.transpose()*A*V)}");

		/* PARTICLE IN A BOX ------------------------------------------------ */
		// generating matrices
		int n = 20;
		matrix H = BoxHamilton(n);
		H.print2stderr("Original box Hamilton: \n");
		V = new matrix(n,n); // redefining V

		// diagonalizing using Cyclic Jacobi
		vector eigval = JDCyclic(H,V);

		// printing particle in a box test output
		WriteLine("\n --- Test of ''particle in a box'' eigenvalue solver --- \n ");
		WriteLine($" Calculation made on a {n}x{n} box Hamilton\n");
		WriteLine("  n | calculated | exact     | % diff \n" + 
				  " -------------------------------------");
		for (int k=0; k < n/3; k++)
		{
		    double exact = PI*PI*(k+1)*(k+1);
		    double calculated = eigval[k];
		    WriteLine(" {0,2} | {1,10:f3} | {2,9:f3} | {3,6:f3}",k ,eigval[k], exact, (exact-eigval[k])/exact);
		}
		/*--------------------------------------------------------------------*/
		WriteLine("\n NOTE: number of rotations and box Hamilton before/after can be seen in log.tbc.txt \n");
		H.print2stderr(" Box Hamilton after calculation");

		return 0;
	}// Main

	static matrix BoxHamilton(int dimension)
	{
		// generating particle in a box hamiltonian
		int n=dimension;
		double s=1.0/(n+1);
		matrix H = new matrix(n,n);
		for(int i=0;i<n-1;i++)
		{
		  matrix.set(H,i,i,-2);
		  matrix.set(H,i,i+1,1);
		  matrix.set(H,i+1,i,1);
		}
		matrix.set(H,n-1,n-1,-2);
		H *= -1/s/s;

		return H;
	}// BoxHamilton

}// main
