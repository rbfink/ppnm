using System;
using static System.Console;
using static System.Math;
using static qr_gs;

static class main
{
	static int Main()
	{
		prob_A1();
		prob_A2();
		prob_B();
		
		return 0;
	}// Main

	static void prob_A1()
	{
		var rng = new Random();
		int m = rng.Next(2,10);
		int n = m + rng.Next(10);
		matrix A = RandomMatrix(n,m);

		// writing test results
		WriteLine("");
		WriteLine(" ----- Part A: Test of Gram-Schmidt orthogonalization -----\n"); 
		A.print($"{n}x{m} Random matrix A :");
		matrix R = new matrix(A.size2,A.size2);
		matrix A_original = A.copy();

		// decomposing A into A=QR. the given A becomes Q
		int decompsteps = qr_gs_decomp(A, R);

		// printing Q and R matrices
		A.print($" {n}x{m} Orthogonal Q matrix :");
		R.print($" {n}x{m} Upper triangular matrix R :");
		WriteLine("");

		// testing whether Q^(T)*Q = 1
		matrix unity = new matrix(m,m);
		unity.set_identity();
		WriteLine($" Steps taken in decomposition = {decompsteps}");
		WriteLine($" Q^(T)*Q = identity -> {unity.equals(A.T*A)}");
		// testing whether QR=A
		WriteLine($" Q*R     = A --------> {A_original.equals(A*R)}");
	}// prob_1

	static void prob_A2()
	{
		WriteLine("\n ----- Part A: Test of linear equation solver using backsubstitution (Ax=b) -----\n");
		var rng = new Random();
		int m = rng.Next(2,15);
		matrix A = RandomMatrix(m,m);
		matrix R = new matrix(m,m);
		vector b = RandomVector(m);
		matrix A_original = A.copy();
				
		qr_gs_decomp(A,R);
		vector result = qr_gs_solve(A,R,b);
		WriteLine($" Ax=b ---> {(A_original*result).approx(b)}");
		b.print(" b 		   = ");
		(A_original*result).print(" Ax 		   = ");
		(A_original*result-b).print(" Ax-b 	  	   = ");
	}

	static void prob_B()
	{
		WriteLine("\n ----- Part B: Test of Gram Schmidt square matrix inverter -----\n");
		var rng = new System.Random();
		int dim = rng.Next(2,10);
		matrix test = RandomMatrix(dim, dim);
		matrix invtest = qr_gs_inverse(test);

		test.print(" Matrix before being inverted : \n");
		invtest.print("\n Matrix after being inverted B : \n");
		WriteLine("\n Testing whether the inverter works :");
		matrix I = new matrix(dim,dim);
		I.set_identity();
		WriteLine($"\n AB=I ---> {(invtest*test).equals(I)} \n");
	}

	private static matrix RandomMatrix(int n, int m)
	{
		// generating random matrix
		var M_rng = new System.Random();
		matrix A = new matrix(n,m);
		for (int row=0; row<n; row++)
		{
			for (int col=0; col<m; col++)
			{
				A[row,col] = M_rng.NextDouble()*10;
			}
		}
		return A;
	}

	private static vector RandomVector(int n)
	{
		// generating random vector
		var V_rng = new Random();
		vector rv = new vector(n);
		for (int i=0; i<n; i++)
		{
			rv[i] = V_rng.NextDouble()*10;
		}
		return rv;
	}
}// main
