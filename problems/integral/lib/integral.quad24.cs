using System;
using static System.Math;
using static misc;

public static partial class integral
{
	public static double quad24
	(
		Func<double,double> f,
		double a, /* start */
		double b, /* end */
		double acc=1e-4, /* local */
		double eps=1e-4 /* absolute */
	)
	{
		return adapt(f,a,b,acc,eps);
	}

	private static double adapt
	(Func<double,double> f, double a, double b, double acc, double eps)
	{
		double f2 = f(a+2*(b-a)/6);
		double f3 = f(a+4*(b-a)/6);
		int nrec = 0;
		
		return adapt24(f,a,b,acc,eps,f2,f3,nrec);
	}// adapt

	private static double adapt24
	(Func<double,double> f, double a, double b, double acc, double eps,
	 double f2, double f3, int nrec)
	{		
		double f1 = f(a+(b-a)/6);
		double f4 = f(a+5*(b-a)/6);
		double Q  = (2*f1+f2+f3+2*f4)/6*(b-a);
		double q  = (f1+f2+f3+f4)/4*(b-a);
		double tolerance = acc+eps*dabs(Q);
		double error = dabs(Q-q);

		if (error < tolerance)
		{
			return Q;
		}
		else 
		{
			double Q1 = adapt24(f,a,(a+b)/2,acc/Sqrt(2.0),eps,f1,f2,nrec+1);
			double Q2 = adapt24(f,(a+b)/2,b,acc/Sqrt(2.0),eps,f3,f4,nrec+1);
			return Q1+Q2;
		}
	}// adapt24
}// integral
