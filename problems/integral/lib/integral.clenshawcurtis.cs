using System;
using static System.Math;
using static misc;

public static partial class integral
{
	public static double ClenshawCurtis
	(
		Func<double,double> f,
		double a,
		double b,
		double acc=1e-4,
		double eps=1e-4
	)
	{
		Func<double, double> u = (x) => f((b-a)/2 * x + (b+a)/2)*(b-a)/2;
		/* Clenshaw Curtis transformation */
		Func<double, double> f_CC = (theta) => f(Cos(theta))*Sin(theta);
		/* Integration using transformed coordinates */
		return quad24(f_CC, 0, PI/2);
	}
}
