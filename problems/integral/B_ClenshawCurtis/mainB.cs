using System;
using static System.Math;
using static System.Console;

public static class main
{
	public static int Main()
	{
		Func<double,double> A1 = (x) => 4*Math.Sqrt(1-x*x);
		Func<double,double> A2 = (x) => 1/(Sqrt(x));
		Func<double,double> A3 = (x) => Log(x)/(Sqrt(x));

		double A1_ex  = Math.PI;
		double A1_int = integral.ClenshawCurtis(A1,0,1);
		double A2_ex  = 2.0;
		double A2_int = integral.ClenshawCurtis(A2,0,1);
		double A3_ex  = -4.0;
		double A3_int = integral.ClenshawCurtis(A3,0,1);

		WriteLine("\n ---- Integration part B ----");
		WriteLine(" Using Clenshaw Curtis... I seem to have made the\n transformation with a factor 2. See ../lib/integral.clenshawcurtis.cs\n");
		WriteLine(" accuracy of integrator  : 1e-4\n");
		WriteLine(" int_(0)^(1) 4*sqrt(1-x*x) = {0}", A1_int);
		WriteLine(" analytical result = pi    = {0}", A1_ex);
		WriteLine(" difference                = {0}\n", A1_int-A1_ex);

		WriteLine(" int_(0)^(1) 1/(Sqrt(x))   = {0}", A2_int);
		WriteLine(" analytical result  = 2    = {0}", A2_ex);
		WriteLine(" difference                = {0}\n", A2_int-A2_ex);

		WriteLine(" int_(0)^(1) Ln(x)/(Sqrt(x)) = {0}", A3_int);
		WriteLine(" analytical result = -4      = {0}", A3_ex);
		WriteLine(" difference                  = {0}\n", A3_int-A3_ex);


		return 0;		
	}
}
