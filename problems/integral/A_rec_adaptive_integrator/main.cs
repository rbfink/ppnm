using System;
using static System.Console;

public static class main
{
	public static int Main()
	{
		Func<double,double> A1 = (x) => Math.Sqrt(x);
		Func<double,double> A2 = (x) => 4*Math.Sqrt(1-x*x);
		Func<double,double> A3 = (x) => x*x;

		double A1_ex  = 2.0/3;
		double A1_int = integral.quad24(A1,0,1);

		double A2_ex  = Math.PI;
		double A2_int = integral.quad24(A2,0,1);

		double A3_ex  = 1f/3;
		double A3_int = integral.quad24(A3,0,1);

		double A4_ex  = 125f/3;
		double A4_int = integral.quad24(A3,0,5);

		WriteLine("\n ---- Integration part A ----");
		WriteLine(" using rectangular and trapezoidal weights, reusing points.");
		WriteLine(" accuracy of integrator  : 1e-4\n");
		WriteLine(" int_(0)^(1) sqrt(x)       = {0}", A1_int);
		WriteLine(" analytical result = 2/3   = {0}", A1_ex);
		WriteLine(" difference                = {0}\n", A1_int-A1_ex);

		WriteLine(" int_(0)^(1) 4*sqrt(1-x*x) = {0}", A2_int);
		WriteLine(" analytical result = pi    = {0}", A2_ex);
		WriteLine(" difference                = {0}\n", A2_int-A2_ex);

		WriteLine(" int_(0)^(1) x*x           = {0}", A3_int);
		WriteLine(" analytical result         = {0}", A3_ex);
		WriteLine(" difference                = {0}\n", A3_int-A3_ex);

		WriteLine(" int_(0)^(5) x*x           = {0}", A4_int);
		WriteLine(" analytical result         = {0}", A4_ex);
		WriteLine(" difference                = {0}\n", A4_int-A4_ex);
		
		return 0;		
	}
}
