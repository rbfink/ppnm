using static System.Math;

class datagen
{
	public static void Main()
	{
		System.IO.TextWriter stderr = System.Console.Error;
		double max = 4*PI;
		double delta = PI/5;
		for (double i=0.0; i<max+delta; i+=delta)
		{
			System.Console.WriteLine($"{i} {Cos(i)}");
		}
	}// Main
}// main
