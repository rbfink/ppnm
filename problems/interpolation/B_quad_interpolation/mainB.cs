using System;
using static System.Console;
using static Interpolation;

class main
{
	static int Main(string[] args)
	{
		/* test data */
		double[] xt = new double[] {1,2,3,4,5};
		double[] y1 = new double[] {1,1,1,1,1};
		double[] y2 = new double[] {1,2,3,4,5};
		double[] y3 = new double[] {1,4,9,16,25};

		/* tabulated data */
		double[][] data = DataReader.ReadFile2da(args[0]);
		double[] tabx = data[0];
		double[] taby = data[1];

		/* interpolating test functions */
		Func<double,double> QI1   = qspline(xt, y1, 0);
		Func<double,double> QD1   = qspline(xt, y1, 1);
		Func<double,double> QInt1 = qspline(xt, y1, 2);
		Func<double,double> QI2   = qspline(xt, y2, 0);
		Func<double,double> QD2   = qspline(xt, y2, 1);
		Func<double,double> QInt2 = qspline(xt, y2, 2);
		Func<double,double> QI3   = qspline(xt, y3, 0);
		Func<double,double> QD3   = qspline(xt, y3, 1);
		Func<double,double> QInt3 = qspline(xt, y3, 2);

		/* Qspline for Cos function (containing p_i,b_i and c_i's) */
		Func<double,double> Quad_interpol = qspline(tabx,taby,0);
		Func<double,double> Quad_deriv    = qspline(tabx,taby,1);
		Func<double,double> Quad_integral = qspline(tabx,taby,2);

		/* writing data */
		System.IO.TextWriter stderr = Console.Error;
		double start = tabx[0];
		double slut  = tabx[tabx.Length-1];
		double dz = 0.01;
		for (double z=start; z<slut; z+=dz)
		{
			WriteLine($"{z} {Quad_interpol(z)} {Quad_deriv(z)} {Quad_integral(z)}");
		}
		start = xt[0];
		slut = xt[xt.Length-1];
		for (double x=start; x<slut; x+=dz)
		{
			stderr.WriteLine($"{x} {QI1(x)} {QD1(x)} {QInt1(x)} {QI2(x)} {QD2(x)} {QInt2(x)} {QI3(x)} {QD3(x)} {QInt3(x)}");
		}

		return 0;
	}
}
