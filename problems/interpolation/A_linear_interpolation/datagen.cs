class datagen
{
	public static void Main()
	{
		System.IO.TextWriter stderr = System.Console.Error;
		double max = 4.0*System.Math.PI;
		double delta = System.Math.PI/5.0;
		for (double i=0; i<max+delta; i+=delta)
		{
			System.Console.WriteLine("{0} {1}", i, System.Math.Cos(i));
		}
	}// Main
}// main
