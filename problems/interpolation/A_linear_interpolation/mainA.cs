using static System.Console;
using static Interpolation;

class main
{
	public static int Main(string[] args)
	{
		System.IO.TextWriter stderr = Error;
		/* ------------------------------------------------------------------ */
		/* read tabulated data */
		double[][] data = DataReader.ReadFile2da(args[0]);
		double[] tabx = data[0];
		double[] taby = data[1];

		/* write results */
		double interp;
		double integral;
		double start = tabx[0];
		double slut  = tabx[tabx.Length-1];
		double delta = 0.01;
		for (double z=start; z<slut; z+=delta)
		{
			interp   = LinInterp(tabx, taby, z);
			integral = LinInterpIntegral(tabx,taby,z);
			WriteLine($"{z} {interp} {integral}");
		}
		
		return 0;
	}
}
