// using partial class to create one big interpolation class
public static partial class Interpolation
{
	public static int BinarySearch(double[] xs, double target)
	{/*-----------------------------------------------------------------------*/
	 // Binary search of ordered double[]: xs = [min,...........,max]
	 // Returns index of the leftmost index relative to target, however if
	 // target==xs[i] then the i-1 index is returned (except in the left edge case)
	 /*-----------------------------------------------------------------------*/
		int i = 0;
		int j = xs.Length-1;
		while (j-i>1)
		{
			int m = (i+j)/2;
			if (target -xs[m]>0)
			{i = m;}
			else
			{j=m;}
		}//while
		return i;
	}//BinarySearch
}//Interpolation class
