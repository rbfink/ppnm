public static partial class Interpolation
{
	public static double LinInterp(double[] xs
								  ,double[] ys
								  ,double target)
	{
		//the index of x_i, where x_i<target<x_i+1
		int i = BinarySearch(xs, target);
		return ys[i] +(ys[i+1]-ys[i])/(xs[i+1]-xs[i])*(target-xs[i]);
	}//LinInterp

	public static double LinInterpIntegral(double[] xs
										  ,double[] ys
										  ,double target)
	{
		int t_i = BinarySearch(xs, target);
		double integral = 0.0;
		// summing the integral up until the interval x_i<target<x_i+1
		for (int i=0; i<t_i; i++)
		{
			double x_i = xs[i+1] - xs[i];
			double p_i = (ys[i+1]-ys[i])/x_i;
			integral += ys[i]*x_i + (1.0/2)*p_i*x_i*x_i;
		}
		// adding the last part of the integral
		double p_t_i = (ys[t_i+1]-ys[t_i])/(xs[t_i+1]-xs[t_i]);
		double x_t   = target-xs[t_i];
		integral += ys[t_i]*x_t + (1.0/2)*p_t_i*(x_t*x_t);
		return integral;
	}//LinInterpIntegral
		
}//Interpolation
