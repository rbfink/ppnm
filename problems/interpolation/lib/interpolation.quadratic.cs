using System;
public static partial class Interpolation
{
	public static Func<double[], double[], int, Func<double,double>> qspline
	= delegate(double[] xs, double[] ys, int deriv)
	{
		// declaring p_i,c_i and b_i containers
		double[] P = new double[xs.Length-1];
		double[] C = new double[xs.Length-1];
		double[] B = new double[xs.Length-1];
		// calculating p_i's
		for (int i=0; i<P.Length; i++)
		{
			P[i] = (ys[i+1]-ys[i])/(xs[i+1]-xs[i]);
		}
		// calculating c_i's
		C[0] = 0.0;
		for (int i=0; i<C.Length-1; i++)
		{
			C[i+1] = 1/(xs[i+2]-xs[i+1])*( P[i+1]-P[i]-C[i]*(xs[i+1]-xs[i]) );
		}
		C[C.Length-1] = C[C.Length-1]/2;
		for (int i=C.Length-2; i>0; i--)
		{
			C[i] = 1/(xs[i+1]-xs[i])*( P[i+1]-P[i]-C[i]*(xs[i+2]-xs[i+1]) );
		}
		// calculating b_i's
		for (int i=0; i<B.Length; i++)
		{
			B[i] = P[i]-C[i]*(xs[i+1]-xs[i]);
		}

		// function that returns the requested output (defined by "deriv" parameter)
		Func<double,double> spline = delegate(double target)
		{
			int i = BinarySearch(xs, target);
			double deltaz = (target-xs[i]);
			// returning deriviative
			if (deriv==1)
			{return B[i]+2*C[i]*deltaz;}
			// returning integral
			if (deriv==2)
			{
				double integral = 0.0;
				for (int k=0; k<i; k++)
				{
					double del = xs[k+1]-xs[k];
					integral += ys[k]*del+1.0/2*B[k]*del*del+1.0/3*C[k]*del*del*del;
				}
				integral += ys[i]*deltaz+1.0/2*B[i]*deltaz*deltaz+1.0/3*C[i]*deltaz*deltaz*deltaz;
				return integral;
			}
			// returning the spline
			else
			{return ys[i]+B[i]*deltaz+C[i]*deltaz*deltaz;}
		};// spline

		// output is a function		
		return spline;

	};// qspline	
}// Interpolation
