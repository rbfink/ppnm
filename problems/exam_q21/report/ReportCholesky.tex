\documentclass[a4paper,oneside]{memoir}
\usepackage{graphicx}
\usepackage{mathtools}

%changing numbering of figures from 0.1 to 1 and so forth
\renewcommand\thefigure{\arabic{figure}}
\setcounter{figure}{0}

\title{PPNM Exam project 2020\\ Cholesky decomposition}
\author{Rasmus Burlund Fink}

\begin{document}
\maketitle

\noindent In this exam project, the Cholesky matrix decomposition method was implemented and
benchmarked with respect to the $QR$ Gram Smith decomposition implemented earlier
in the course. Furthermore, a linear equation solver, matrix inverse and 
determinant calculator was implemented based on the Cholesky decomposition.

\subsection*{Cholesky decomposition method}
The Cholesky decomposition for real symmetric positive-definite matrices is on the form
\begin{align*}
\mathbf{A} = \mathbf{LL^T}
\end{align*}
Where the matrix $L$ is lower triangular. For a matrix $A$ with the above mentioned 
characteristics, the coresponding $L$ matrix will be unique.
For a $3\times3$ matrix, the following must hold.

\begin{align*}
\mathbf{A}=\mathbf{L} \mathbf{L}^{T}&=\begin{bmatrix}
L_{11} & 0 & 0 \\
L_{21} & L_{22} & 0 \\
L_{31} & L_{32} & L_{33}
\end{bmatrix}\begin{bmatrix}
L_{11} & L_{21} & L_{31} \\
0 & L_{22} & L_{32} \\
0 & 0 & L_{33}
\end{bmatrix}\\
&=\begin{bmatrix}
L_{11}^{2} &  & (\text{ symmetric }) \\
L_{21} L_{11} & L_{21}^{2}+L_{22}^{2} &  \\
L_{31} L_{11} & L_{31} L_{21}+L_{32} L_{22} & L_{31}^{2}+L_{32}^{2}+L_{33}^{2}
\end{bmatrix}
\end{align*}

Which results in the following description of the $L$ matrix.

\begin{align*}
\mathbf{L} = \begin{bmatrix}
\sqrt{A_{11}} & 0 & 0 \\
A_{21} / L_{11} & \sqrt{A_{22}-L_{21}^{2}} & 0 \\
A_{31} / L_{11} & \left(A_{32}-L_{31} L_{21}\right) / L_{22} & \sqrt{A_{33}-L_{31}^{2}-L_{32}^{2}}
\end{bmatrix}
\end{align*}
Generalizing this result to a general $n \times n$ matrix, the following 
algorithm is found

\begin{align*}
L_{j j}=\sqrt{A_{j j}-\sum_{k=1}^{j-1} L_{j k}^{2}}\, ,\quad L_{i j}=\left.\frac{1}{L_{j j}}\left(A_{i j}-\sum_{k=1}^{j-1} L_{i k} L_{j k}\right)\right|_{i>j}
\end{align*}
This algorithm can be implemented in two ways: Cholesky-Banachiewicz (row by row), 
or Cholesky–Crout (column by column). I chose to implement the former.\\

\noindent The Cholesky decomposition method with Cholesky-Banachiewicz algorithm can be found 
in the following location: \\
\begin{center}
\texttt{ppnm/problems/exam\_q21/lib/cholesky.decomp.cs}
\end{center}

\subsection*{Linear equation solver}
The implementaion of linear equation solver was done using the general procedure
for solving $Ax=b$ problems, where $A$ can be decomposed as $A=LU$, where $L$ and $U$
are lower and upper triangular respectively.
Such a system can be solved by first solving $Ly=b$ by forward substitution, and
then solving $Ux=y$ with back-substitution. The Cholesky decomposition is of
course also of the $LU$ type, only where $U=L^T$.\\

\noindent The linear equation implementation can be found in the following location:\\
\begin{center}
\texttt{ppnm/problems/exam\_q21/lib/cholesky.solve.cs}
\end{center}

\subsection*{Matrix inverse calculator}
The inverse $A^{-1} \Rightarrow AA^{-1}=I$ for a $n \times n$ can be calculated by
solving the following $n$ linear eqautions
\begin{align}
\mathbf{A} \mathbf{x}_{i}=\left.\mathbf{e}_{i}\right|_{i=1, \ldots, n}
\end{align}
where $e_i$ is the unitvector in the i'th direction.\\

\noindent The matrix inverse implementation can be found at the following location:\\

\begin{center}
\texttt{ppnm/problems/exam\_q21/lib/cholesky.inverter.cs}
\end{center}

\subsection*{Matrix determinant}
The determinant of a product of matrices is equal to the product of matrix 
determinants. This results in the following formula
\begin{align*}
\operatorname{det} \mathbf{A}=\operatorname{det} \mathbf{L} \mathbf{U}=\operatorname{det} \mathbf{L} \operatorname{det} \mathbf{U}=\prod_{i=1}^{n} L_{i i}U_{i i}
\end{align*}

For the Cholesky decomposition $A=LL^T$, this means that 

\begin{align*}
\operatorname{det} \mathbf{A} = \operatorname{det} \mathbf{L} \operatorname{det} \mathbf{L^T}=\prod_{i=1}^{n} L_{i i}^2
\end{align*}
Due to the fact that the lower triangular $L$ and it's transpose $L^T$ share the
same diagonal elements.\\

\noindent The matrix determinant implementation can be found at the following location:\\

\begin{center}
\texttt{ppnm/problems/exam\_q21/lib/cholesky.determinant.cs}
\end{center}

\subsection*{Peformance}
The performance of the Cholesky method was benchmarked relative to the modified
Gram Schmidt $QR$ decomposition implemented earlier in the course, and can be found
at: \texttt{ppnm/problems/linear\_equations/lib/qr\_gs.cs}. The benchmark was done
using random symmetric positive definite matrices of growing dimension, and each
operation was counted as well as the time duration of the decomposition.\\
For lower dimensions, up to $\approx 110$ the two methods have comparable performance.
But for higher dimensions, the Cholesky method wins in both regards. For the 
highest dimension in the benchmark $n=600$, the time differnce nears $1$ second.
The results of the benchmark can be seen in the figures below

\begin{figure}[ht]
	\centering
	\input{plot-cost.tex}
	\caption{Computational cost relative to matrix dimension. Both
		methods has a complexity of $O(n^{3})$. However, the Cholesky ($LL$)
		decomposition is approximately three times faster than the Gram Schmidt 
		$QR$ method, as can be seen from the indicative lines.
		 For a logarithmic plot, see figure \ref{fig:costlog}}
	\label{fig:cost}
\end{figure}
\begin{figure}[ht]
	\centering
	\input{plot-logcost.tex}
	\caption{Computational cost relative to matrix dimension. Both
	methods has a complexity of $O(n^{3})$. However, the Cholesky ($LL$)
	decomposition is approximately three times faster than the Gram Schmidt $QR$ method, as can be seen from the
	indicative lines. For a non logarithmic plot, see figure \ref{fig:cost}}
	\label{fig:costlog}
\end{figure}
\begin{figure}[ht]
	\centering
	\input{plot-time.tex}
	\caption{Computation time of decomposition relative to matrix dimension. Again
	it can be seen that the Cholesky decomposition is faster, as would be expected
	from the computational complexity seen in figure \ref{fig:cost} and \ref{fig:costlog}}
	\label{fig:time}
\end{figure}

\end{document}
