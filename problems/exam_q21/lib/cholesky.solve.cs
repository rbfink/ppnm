using static System.Console;

public static partial class Cholesky
{
	public static int Solver(matrix L, vector b, vector x)
	{
		/* error handling */
		if (L.size1!=L.size2)
		{throw new System.ArgumentException("\n CholeskySolver: L is not symetric");}
		if (b.size!=L.size1)
		{throw new System.ArgumentException("\n CholeskySolver: vector b is not the right size");}
		if (x.size!=b.size)
		{throw new System.ArgumentException("\n CholeskySolver: x.size!=b.size");}
		/*
			Solving the linear system Ax=b => (LL^T)x=b by solving Ly=b with
			forward substitution, then L^Tx=y with backsubstitution
		*/
		matrix LT = L.copy().transpose();
		int dim = b.size;
		vector y = new vector(dim);
		x.set_zero();
		double sum;
		int steps = 0;
		
		/* solving Ly=b with forward substitution */
		for (int i=0; i<=dim-1; i++)
		{
			steps++;
			sum = 0.0;
			for (int k=0; k<=i-1; k++)
			{
				sum += L[i,k]*y[k]; // remember points ?
			}
			y[i] = (1/L[i,i])*b[i] - (1/L[i,i])*sum;
		}

		/* solving L.Tx=y with back-substitution */
		for (int i=dim-1; i>=0; i--)
		{
			steps++;
			sum = 0.0;
			for (int k=i+1; k<dim; k++)
			{
				sum += LT[i,k]*x[k];
			}
			x[i] = (1/LT[i,i])*y[i] - (1/LT[i,i])*sum;
		}
		return steps;
	}// Cholesky solver
}// Choleskydecomp
