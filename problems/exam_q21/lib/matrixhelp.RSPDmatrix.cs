using System;

public static partial class matrixhelp
{
	public static matrix RSPDmatrix(int dim)
	{
		matrix A = RM(dim,dim);
		matrix I = new matrix(dim,dim);
		I.set_identity();
		A = 0.5*(A*A.T) + dim*I;
		return A;
	}

	private static matrix RM(int n, int m)
	{
		// generating random matrix
		var M_rng = new System.Random();
		matrix A = new matrix(n,m);
		for (int row=0; row<n; row++)
		{
			for (int col=0; col<m; col++)
			{
				A[row,col] = M_rng.Next(5);
			}
		}
		return A;
	}// RandomMatrix

}
