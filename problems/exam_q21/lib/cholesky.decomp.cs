using static System.Math;
using static System.Console;

public static partial class Cholesky
{
	public static int Decomp(matrix A, matrix L, int maxsteps=1000)
	{
		/* Error handling */
		if (A.size1!=A.size2)
		{throw new System.ArgumentException("\n CBLLdecomp: A is not symetric");}
		if (L.size1!=A.size1 | L.size2!=A.size2)
		{throw new System.ArgumentException("\n CBLLdecomp: The dimension of L != A");}
		/* variables */
		int dim = A.size1;
		int steps = 0;
		/* Cholesky-Banaschiewicz (row by row) in place decoposition */
		for (int i=0; i<dim; i++)
		{
			if (steps>maxsteps)
			{break;}
			for (int j=0; j<=i; j++)
			{
				if (steps>maxsteps)
				{break;}
				/* diagonal */
				if (i==j)
				{
					double diagsum = 0.0;
					for (int k=0; k<j; k++)
					{
						steps++;
						diagsum += L[j,k]*L[j,k];
					}
					steps++;
					L[j,j] = Sqrt(A[j,j] - diagsum);
				}
				/* lower diagonal */
				if (i>j)
				{
					double offdiagsum = 0.0;
					for (int k=0; k<j; k++)
					{
						steps++;
						offdiagsum += L[i,k]*L[j,k];
					}
					steps++;
					L[i,j] = (1/L[j,j])*A[i,j] - (1/L[j,j])*offdiagsum;
				}
			}// for j (column)
		}// for i (row)
		return steps;
	}// CBLLdecomp
}// Choleskydecomp
