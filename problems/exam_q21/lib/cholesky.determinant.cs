using static System.Console;

public static partial class Cholesky
{
	public static double Determinant(matrix A, int maxsteps=2000)
	{
		/*	
			Using Cholesky A=LL^T decomposition and due to the determinant
			of a product of matrices are equal to the product of the
			detereminants the following holds for LU=LL^T

				det(A) = det(LU) = det(L)*det(U)
		*/

		/* Error handling */
		if (A.size1!=A.size2)
		{throw new System.ArgumentException("\n CBLLdeterminant: A is not symetric");}

		/* Parameters */
		matrix L = new matrix(A.size1, A.size1);
		double determinant = 1.0;
		int steps = 0;

		/* Decomposing A */
		steps += Cholesky.Decomp(A, L, maxsteps:maxsteps);

		/* Finding the determinant */
		for (int i=0; i<A.size1; i++)
		{
			steps++;
			double LL = L[i,i]*L[i,i];
			determinant = determinant*LL;
		}
		return determinant;
	}// CholeskyDeterminant
}// Cholsesky Methods
