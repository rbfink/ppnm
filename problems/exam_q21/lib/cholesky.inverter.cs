using static System.Console;

public static partial class Cholesky
{
	public static int Inverter(matrix A, matrix inverse, int maxsteps=2000)
	{
		/* Error handling */
		if (A.size1 != A.size2)
		{throw new System.ArgumentException(" CholeskyInverter: This method only works for symmetric posititive definite matrices!");}
		if (inverse.size1 != A.size1 | inverse.size2 != A.size2)
		{throw new System.ArgumentException(" CholeskyInverter: The specified inverse matrix does not match matrix A in dimension !");}
		/* Parameters */
		int dim   = A.size1;
		matrix L  = new matrix(dim, dim);
		vector xi = new vector(dim);
		/* Finding L */
		int steps = Cholesky.Decomp(A, L, maxsteps:maxsteps);
		/* Solving the series of e_i eigenvectors */
		for (int col=0; col<dim; col++)
		{
			vector ei = unitvector(dim,col);
			steps += Cholesky.Solver(L, ei, xi);
			for (int row=0; row<dim; row++)
			{
				steps++;
				inverse[row, col] = xi[row];
			}
		}
		return steps;
	}// CholeskyInverter

	private static vector unitvector(int size, int entrance)
	{
		vector e_i = new vector(size);
		e_i[entrance] = 1;
		return e_i;
	}// unitvector
}// CholeskyDecomp
