using System;
using System.Diagnostics;
using static System.Console;

static class main
{
	static int Main(string[] args)
	{
		if (args[0]=="Ctest")
		{
			GeneralTest();
			return 0;
		}
		if (args[0]=="LLvsQR")
		{
			LLvsQR();
			return 0;
		}
		else
		Error.WriteLine("\n main : no text command given! give 'test' or 'LLvsQR' as argument.");
		return 0;
	}// Main

	private static int GeneralTest()
	{
		/* Parameters */
		int dim = 5;
		matrix A = matrixhelp.RSPDmatrix(dim);
		matrix L = new matrix(A.size1,A.size1);
		matrix inverse = new matrix(A.size1,A.size1);
		matrix I = new matrix(A.size1,A.size1); I.set_identity();
		vector b = matrixhelp.RandomVector(dim);
		vector x = new vector(dim);
		
		/* Doing the decomposition */
		int decompstep = Cholesky.Decomp(A, L);
		/* Solving the linear system*/
		int solvestep  = Cholesky.Solver(L, b, x);
		/* Finding the inverse */
		int inversesteps = Cholesky.Inverter(A, inverse);
		/* Calculating the derterminant */
		double determinant = Cholesky.Determinant(A);

		/* Writing output */	
		WriteLine("\n ----- Testing Cholesky decomposition -----\n");
		A.print($" {dim}x{dim} Random symmetric positive definite matrix A : ");
		L.print($" {dim}x{dim} Lower triangular matrix L from Cholesky decomposition A=LL^T :");
		WriteLine($"\n Steps taken in decomposition: {decompstep}");
		WriteLine($" A = L*L.T --> {A.equals(L*L.T)}\n");

		WriteLine(" ----- Testing Cholesky linear equation solver -----\n");
		WriteLine(" Solving Ax=b linear system by decomposing A=LL^T (using Cholesky algorithm) and solving \n Ly=b with forward substitution and then L^Tx=y with backward substitution.");
		WriteLine($"\n Steps taken in solver: {solvestep}");
		WriteLine($" Ax = b --> {(A*x).approx(b)}");
		b.print(" b vector : ");
		(A*x).print(" Ax       : ");
		(A*x-b).print(" Ax-b     : ");

		WriteLine("\n ----- Testing the Choleksy Inverter -----\n");
		inverse.print(" Inverse matrix to A : ");
		WriteLine($"\n steps taken in inverter (including decomp and solving) : {inversesteps}");
		WriteLine($" A*A^(-1) = I --> {(A*inverse).equals(I)}");

		WriteLine("\n ----- Testing the Cholesky Determinant calculator -----\n");
		WriteLine(" Determinant of of the A matrix posted above calculated as\n 	det(A) = det(LL^T) = det(L)det(L^T)");
		WriteLine($"\n steps taken in determinant calculator (including decomp) : {inversesteps}");
		WriteLine($" det(A) = {determinant}\n");
		return 0;
	}// GeneralTest

	private static int LLvsQR()
	{
		Stopwatch sw = new System.Diagnostics.Stopwatch(); // timer method
		long QRsteps, QRtime, LLsteps, LLtime;

		var DataWriter = new System.IO.StreamWriter("timetestdata.tbc.txt");
		for (int dim=20; dim<=600; dim+=20)
		{
			matrix A = matrixhelp.RSPDmatrix(dim);
			matrix L = new matrix(dim, dim);
			matrix R = new matrix(dim, dim);
			/* Cholesky decomposition */
			sw.Start();
			LLsteps = Cholesky.Decomp(A, L, maxsteps:2147483647); //maxint
			sw.Stop();
			LLtime = sw.ElapsedMilliseconds;
			sw.Reset();
			/* Gram-Smith QR decomposition */
			sw.Start();
			QRsteps = qr_gs.qr_gs_decomp(A, R);
			sw.Stop();
			QRtime = sw.ElapsedMilliseconds;
			sw.Reset();
			DataWriter.WriteLine("{0} {1} {2} {3} {4}", dim, LLsteps, QRsteps, LLtime, QRtime);
		}
		DataWriter.Close();
		return 0;
	}// LLvsQR
}// Main
