using System;
using static System.Console;

public static partial class MonteCarlo
{
	public static vector MonteCarloPlain(
		Func<vector,double> f /* function to be integratet */
		,vector a /* lower limits */
		,vector b /* upper limits */
		,int N) /* number of random points */
	{
		/* paraphrasing the code from the problem page... */

		/* Volume */
		double volume =1.0;
		for (int i=0; i<a.size; i++)
		{
			volume *= b[i]-a[i];
		}

		/*  */
		double sum  = 0.0;
		double sum2 = 0.0;
		for (int i=0; i<N; i++)
		{
			var fx = f(ranx(a,b));
			sum += fx;
			sum2 += fx*fx;
		}
		double mean = sum/N;
		double sigma = Math.Sqrt(sum2/N - mean*mean);
		double SIGMA = sigma/Math.Sqrt(N);

		return new vector(mean*volume, SIGMA*volume);
	}// MonteCarloPlain

	private static vector ranx(vector a, vector b)
	{
		var ran = new Random();
		vector ranreturn = new vector(a.size);
		for (int i=0; i<a.size; i++)
		{
			ranreturn[i] = a[i] + ran.NextDouble()*(b[i]-a[i]);
		}
		return ranreturn;
	} // ranx	
	
}// MonteCarlo
