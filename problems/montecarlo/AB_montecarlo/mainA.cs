using System;
using System.Collections.Generic;
using static System.Math;

static class main
{
	static int Main()
	{
		/* Singular test function */
		Func<vector,double> test_func = (v) =>
		{
			return 1.0/((PI*PI*PI)*(1-Cos(v[0])*Cos(v[1])*Cos(v[2])));
		};
		vector testlower = new vector(0, 0, 0);
		vector testupper = new vector(PI, PI, PI);

		int Ntest = 1000000;
		double ar = 1.3932039296856768591842462603255;
		vector testresult = MonteCarlo.MonteCarloPlain(test_func,testlower,testupper,Ntest);

		var testout = new System.IO.StreamWriter("ResultA.tbc.txt");
		testout.WriteLine("\n test function -> Gamma(1/4)^4/(4*pi^3)");
		testout.WriteLine($" test function result            = {testresult[0]}({testresult[1]})");
		testout.WriteLine($" test function analytical result = {ar}");
		testout.WriteLine($" difference                      = {Abs(testresult[0]-ar)}\n");
		testout.Close();

		/* estimation of pi */
		Func<vector,double> pi_aprox = (v) => 
		{
			if(v[0]*v[0]+v[1]*v[1] <= 1.0) {return 1.0;}
			else {return 0.0;}
		};
		vector pilower = new vector(-1, -1);
		vector piupper = new vector(1, 1);

		int points = 200;
		int[] Ns = new int[points];
		vector[] pi_test = new vector[points];
		for (int i=0; i<Ns.Length; i++)
		{
			if (i==0)
			{
				Ns[i] = 1000;
				pi_test[i] = (MonteCarlo.MonteCarloPlain(pi_aprox,pilower,piupper,Ns[i]));
			}
			Ns[i] = (i+1)*1000;
			pi_test[i] = (MonteCarlo.MonteCarloPlain(pi_aprox,pilower,piupper,Ns[i]));
		}

		var datadump = new System.IO.StreamWriter("pi_converge.tbc.txt");
		for (int i=0; i<Ns.Length; i++)
		{
			datadump.WriteLine($"{Ns[i]} {pi_test[i][0]} {pi_test[i][1]}");
		}
		datadump.Close();

		return 0;
	}
}
