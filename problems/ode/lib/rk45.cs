using System;
using System.Collections.Generic;
using static System.Math;
using static vector;

public partial class rkode
{
	public static int rk45
	(
		Func<double,vector,vector> f
		,double a
		,vector y
		,double b
		,double h=0.1
		,List<double> xlist=null
		,List<vector> ylist=null
		,double acc=1e-5
		,double eps=1e-5
	)
	{
		return rk45driver(f, a, y, b, h, xlist, ylist, acc, eps);
	}// rk45

	private static vector[] rk45step
	(
		Func<double,vector,vector> f
		,double t
		,vector yt
		,double h
	)
	{
		// K_s = h*f*(x_n + c_s*h, y_n+a_s1*K_1+a_s2*K_2 + ... +a_ss-1*K_s-1)

		vector k1 = f(t,yt);
		vector k2 = f(t + 1.0/4*h  , yt + 1.0/4*k1*h);
		vector k3 = f(t + 3.0/8*h  , yt + 3.0/32*k1*h + 9.0/32*k2*h);
		vector k4 = f(t + 12.0/13*h, yt + 1932.0/2197*k1*h - 7200.0/2197*k2*h + 7296.0/2197*k3*h);
		vector k5 = f(t + 1*h      , yt + 439.0/216*k1*h - 8*k2*h + 3680.0/513*k3*h - 845.0/4104*k4*h);
		vector k6 = f(t + 1.0/2*h  , yt - 8.0/27*k1*h + 2*k2*h - 3544.0/2565*k3*h + 1859.0/4104*k4*h - 11.0/40*k5*h);

		vector[] ks  = new vector[] {k1, k2, k3, k4, k5, k6};
		double[] b5s = new double[] {16.0/135, 0, 6656.0/12825, 28561.0/56430, -9.0/50, 2.0/55};
		double[] b4s = new double[] {25.0/216, 0, 1408.0/2565, 2197.0/4104, -1.0/5, 0};

		vector yh = yt.copy();
		vector err = new vector(yt.size);
		for (int i=0; i<b5s.Length; i++)
		{
			yh  += h*b5s[i]*ks[i];
			err += h*(b5s[i] - b4s[i])*ks[i];
		}
		
		return new vector[] {yh, err};
	}// rk45step

	private static int rk45driver
	(
		Func<double,vector,vector> f
		,double a
		,vector y
		,double b
		,double h
		,List<double> xs
		,List<vector> ys
		,double acc
		,double eps
	)
	{
		System.IO.TextWriter stderr = System.Console.Error;

		/* putting initial values in data containers */
		xs.Add(a);
		ys.Add(y);
		
		/* declaring variable containers and values */
		vector yh     = new vector(y.size); /* container for function values */
		vector err    = new vector(y.size); /* container for error values */
		double xi     = a;                  /* starting position */
		vector yi     = y.copy();           /* starting function values */
		double step   = h;                  /* starting step size */
		double power  = 0.25;
		double safety = 0.95;
		bool accepted;
		int steps = 0;
		int failsteps = 0;

		while (steps<1000)
		{
			/* taking step */
			vector[] stepresult = rk45step(f, xi, yi, step);//, yh, err);
			yh  = stepresult[0];
			err = stepresult[1];

			/* asserting local tol and err */
			vector ltol = (eps*yh.abs()+acc) * Sqrt(step/(b-a));

			accepted = true;
			vector ratios = new vector(err.size);
			for (int i=0; i<ltol.size; i++)
			{
				if (Abs(err[i]) > Abs(ltol[i]))
				{accepted = false;}
				ratios[i] = Abs(ltol[i]/err[i]);
			}			
			
			/* last step catcher */
			if (xi+step==b && accepted)
			{
				stderr.WriteLine($" Last step catcher: Step accepted = {accepted}\n");

				xi = xi + step;
				yi = yh;
				xs.Add(xi);
				ys.Add(yi);

				break;
			}
			/* if the driver oversteps the final value, adjust step-size */
			else if (xi+step > b)
			{
				stderr.WriteLine($" Overstepping catcher: Overstepped by {xi+step - b}");
				stderr.WriteLine(" ");

				step = b - xi;
			}
			/* if err is accepted, append values and take the next step */
			else if (accepted)
			{
				if (failsteps>0)
				{
					stderr.WriteLine($" ------- step {steps} ------");
					stderr.WriteLine($" xi       =  {xi}");
					stderr.WriteLine($" stepsize =  {step}");
					stderr.WriteLine($" failed steps : {failsteps}");
					stderr.WriteLine("");		
				}	
				steps++;
				failsteps = 0;
				
				xi = xi + step;
				yi = yh;
				xs.Add(xi);
				ys.Add(yi);

				step *= Math.Pow(ratios.min(), power)*safety;
			}
			/* else, adjust stepsize and try again*/
			else
			{
				stderr.WriteLine($" Step not accepted: failed step size = {step}");
				failsteps++;
				step *= Math.Pow(ratios.min(), power)*safety;
			}
		}// while
		return steps;
	}// rk45driver
}// rkode
