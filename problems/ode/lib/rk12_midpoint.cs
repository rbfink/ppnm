using System;
using System.Collections.Generic;
using static vector;

public partial class rkode
{
	// this is the function one calls to integrate the given ODE f
	public static int rk12
	(
		 Func<double,vector,vector> f
		,double a // initial position
		,vector y // initial values of the function: y(a)
		,double b // final position
		,double h=0.1 // initial step-size
		,List<double> xlist=null // positions along the way
		,List<vector> ylist=null // estimated values long the way
		,double acc=1e-1 // absolute accuracy goal
		,double eps=1e-1 // relative accuracy goal
	)
	{
		return rk12driver(f,a,y,b,h,xlist,ylist,acc,eps);
	}// rk12
			
	/* Calculates the step from the given step size and known values and
	   returns the estimated values at y(t+h) and errors

	   Midpoint euler method butcher tableau
			ci's
			0   | 	  ai's
			1/2 | 1/2
		  --------------
		  	    | 0    1
		  	    | 1    0
					bi's
	*/
	private static vector[] rkstep12
	(
		 Func<double,vector,vector> f // function to numerically integrate
		,double t // the "known" position x
		,vector yt // function values at this position
		,double h // step size
	)
	{
		/* estimation variables */
		vector k0  = f(t,yt);
		vector k12 = f(t + 0.5*h, yt + 0.5*h*k0);

		/* estimation of y(t+h) */
		vector yh = yt + h*k12;

		/* "embedded" error estimation */
		vector err = (h*k12 - h*k0);

		return new vector[] {yh, err};
	}// rkstep12

	/* advances the the stepper from  a to b and evaluates the error estimate
	   at each step to check whether it should change step size */
	private static int rk12driver
	(
		 Func<double,vector,vector> f
		,double a // initial position
		,vector y // initial values of the function: y(a)
		,double b // final position
		,double h // initial step-size
		,List<double> xs
		,List<vector> ys
		,double acc // absolute accuracy goal
		,double eps // relative accuracy goal
	)
	{
		System.IO.TextWriter stderr = System.Console.Error;

		/* putting initial values in data containers */
		xs.Add(a);
		ys.Add(y);
		
		/* declaring variable containers and values */
		vector yh     = new vector(y.size); /* container for function values */
		vector err    = new vector(y.size); /* container for error values */
		double xi     = a;                  /* starting position */
		vector yi     = y.copy();           /* starting function values */
		double step   = h;                  /* starting step size */
		double power  = 0.25;
		double safety = 0.95;
		double ltol, lerr;
		bool accepted;
		int steps = 0;
		int failsteps = 0;

		while (steps<1000)
		{
			/* taking step */
			vector[] stepresult = rkstep12(f, xi, yi, step);//, yh, err);
			yh  = stepresult[0];
			err = stepresult[1];

			/* asserting local tol and err */
			double temptol = (eps*yh.norm()+acc) * Math.Sqrt(step/(b-a));
			ltol = dsgn(temptol)*temptol;
			lerr = dsgn(err.norm())*err.norm();

			accepted = (lerr < ltol);

			/* last step catcher */
			if (xi+step==b && accepted)
			{
				stderr.WriteLine($" Last step catcher: Step accepted = {accepted}");

				xi = xi + step;
				yi = yh;
				xs.Add(xi);
				ys.Add(yi);

				break;
			}
			/* if the driver oversteps the final value, adjust step-size */
			else if (xi+step > b)
			{
				stderr.WriteLine($" Overstepping catcher: Overstepped by {xi+step - b}");
				stderr.WriteLine(" ");

				step = b - xi;
			}
			/* if err is accepted, append values and take the next step */
			else if (accepted)
			{
				if (failsteps>0)
				{
					stderr.WriteLine($" ------- step {steps} ------");
					stderr.WriteLine($" xi       =  {xi}");
					stderr.WriteLine($" stepsize =  {step}");
					stderr.WriteLine($" failed steps : {failsteps}");
					stderr.WriteLine("");		
				}	
				steps++;
				failsteps = 0;
				
				xi = xi + step;
				yi = yh;
				xs.Add(xi);
				ys.Add(yi);

				step *= Math.Pow( (ltol/lerr), power)*safety;
			}
			/* else, adjust stepsize and try again*/
			else
			{
				stderr.WriteLine($" Step not accepted: failed step size = {step}");
				failsteps++;
				step *= Math.Pow( (ltol/lerr), power)*safety;
			}
		}// while
		return steps;
	}// driver

	private static int dsgn(double x)
	{
		if (x >= 0)
		{return 1;}
		else
		{return -1;}
	}// dsgn
	
}// ode
