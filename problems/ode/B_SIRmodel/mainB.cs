using System;
using static System.Console;
using System.Collections.Generic;
using static vector;

public static class main
{
	static int Main()
	{
		/* SIR model of infectious disease population spread */
		/* dS/dt = -I*S/N*T_c         | Susceptible */
		/* dI/dt =  I*S/N*T_c - I/T_r | Infectious  */
		/* dR/dt =  I/T_r             | Removed     */
		/* We are working with a coupled system of ODE's */

		/* Data containers and variables */
		List<double> xSIR = new List<double>();
		List<vector> ySIR = new List<vector>();
		double N, I, R, T_c, T_r, start, step, end;
		N = 6e6; /* aproximate population of Denmark */
		I = 500; /* infected at t=0 */
		R = 0; /* removed at t=0*/
		T_c = 8; /* avg. time between contacts */
		T_r = 14; /* avg. recovery time */
		start = 0; /* start of epidemic */
		step = 1; /* initial step is one day	 */
		end  = 365; /* one year later */ 
		vector y0 = new vector(N-I, I, R); /* starting conditions */

		/*  calling the integrator */		
		int SIRsteps = rkode.rk45(dSIR(N,T_c,T_r),start,y0,end,step,xlist:xSIR,ylist:ySIR);
		Error.WriteLine($" steps taken = {SIRsteps}");

		/* writing to a file... */
		var datadump = new System.IO.StreamWriter("outSIR.tbc.txt");
		for (int i=0; i<xSIR.Count; i++)
		{
			datadump.WriteLine($"{xSIR[i]} {ySIR[i][0]} {ySIR[i][1]} {ySIR[i][2]}");
		}
		datadump.Close();

		WriteLine("\n --- Initial conditions of SIR plot ---");
		WriteLine($" N = {N} --> /* aproximate population of Denmark */");
		WriteLine($" I = {I} --> /* infected at t=0 */");
		WriteLine($" R = {R} --> /* removed at t=0*/");
		WriteLine($" T_c = {T_c} --> /* avg. time between contacts */");
		WriteLine($" T_r = {T_r} --> /* avg. recovery time */");
		WriteLine($" start = {start} --> /* start of epidemic */");
		WriteLine($" step = {step} --> /* initial step is one day */");
		WriteLine($" end  = {365} --> /* one year later */\n");
	
		return 0;
	}

	public static Func<double,vector,vector> dSIR(double N, double T_c, double T_r)
	{
		return (x,y) => 
		{
			vector yd = new vector(3);
			yd[0] = -(y[1]*y[0])/(N*T_c);
			yd[1] =  (y[1]*y[0])/(N*T_c) - y[1]/T_r;
			yd[2] =  y[1]/T_r;
			return yd;
		};
	}// dSIR
}// main
