using System;
using  System.Collections.Generic;
using static vector;

class main
{
	static int Main(string[] args)
	{
		if (args[0]=="rk12")
		{return rk12();}
		if (args[0]=="rk45")
		{return rk45();}
		else
		{
			System.Console.Error.WriteLine("\n no ode integrator chosen !");
			return 0;
		}
	}
		static int rk12()
		{
			System.IO.TextWriter stderr = System.Console.Error;
			Func<double, vector, vector> f = (t,yt) => new vector(yt[1], -yt[0]);
//			Func<double, vector, vector> f = (t,yt) => new vector(yt[0]-yt[0]*yt[0], 0);
	
			double start, slut, y_0, dy_0;
			vector ystart = new vector(2);
			List<double> xs12 = new List<double>();
			List<vector> ys12 = new List<vector>();
			start = 0;
			slut  = 2*Math.PI; //3;
			y_0   = 0; //0.5;
			dy_0  = 1; //0.25;
			ystart[0] = y_0;
			ystart[1] = dy_0;
			int rk12steps = rkode.rk12(f, start, ystart, slut, xlist:xs12, ylist:ys12);
			stderr.WriteLine($" rk12 total steps = {rk12steps}");
			for (int i=0; i<xs12.Count; i++)
			{
				Console.WriteLine($"{xs12[i]} {ys12[i][0]} {ys12[i][1]}");
			}
			return 0;
		}

		static int rk45()
		{
			System.IO.TextWriter stderr = System.Console.Error;
			Func<double, vector, vector> f = (t,yt) => new vector(yt[1], -yt[0]);
//			Func<double, vector, vector> f = (t,yt) => new vector(yt[0]-yt[0]*yt[0], 0);
	
			double start, slut, y_0, dy_0;
			vector ystart = new vector(2);
			List<double> xs45 = new List<double>();
			List<vector> ys45 = new List<vector>();
			start = 0;
			slut  = 2*Math.PI; //3;
			y_0   = 0; //0.5;
			dy_0  = 1; //0.25;
			ystart[0] = y_0;
			ystart[1] = dy_0;
			int rk45steps = rkode.rk45(f, start, ystart, slut, xlist:xs45, ylist:ys45);
			stderr.WriteLine($" rk45 total steps = {rk45steps}");
			for (int i=0; i<xs45.Count; i++)
			{
				Console.WriteLine($"{xs45[i]} {ys45[i][0]} {ys45[i][1]}");
			}	
			return 0;
		}
}


//		System.IO.TextWriter stderr = System.Console.Error;
//		Func<double, vector, vector> f = (t,yt) => new vector(yt[1], -yt[0]);
//		Func<double, vector, vector> f = (t,yt) => new vector(yt[0]-yt[0]*yt[0], 0);
//
//		double start, slut, y_0, dy_0;
//		vector ystart = new vector(2);
//		List<double> xs12 = new List<double>();
//		List<vector> ys12 = new List<vector>();
//		List<double> xs45 = new List<double>();
//		List<vector> ys45 = new List<vector>();
//		start = 0;
//		slut  = 2*Math.PI; //3;
//		y_0   = 0; //0.5;
//		dy_0  = 1; //0.25;
//		ystart[0] = y_0;
//		ystart[1] = dy_0;
//
//		int rk12steps = rkode.rk12(f, start, ystart, slut, xlist:xs12, ylist:ys12);
//		int rk45steps = rkode.rk45(f, start, ystart, slut, xlist:xs45, ylist:ys45);
//
//		stderr.WriteLine($" rk12 total steps = {rk12steps}");
//		stderr.WriteLine($" rk45 total steps = {rk45steps}");
//		
//		for (int i=0; i<xs12.Count; i++)
//		{
//			Console.WriteLine($"{xs12[i]} {ys12[i][0]} {ys12[i][1]}");
//		}
//		Console.WriteLine("");
//		for (int i=0; i<xs45.size; i++)
//		{
//			Console.WriteLine("{xs45[i]} {ys45[i][0]} {ys45[i][1]}");			
//		}
//
//		return 0;
//	}// Main
//}// main
