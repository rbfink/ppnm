using System;

public static class main
{
	public static int Main()
	{
		Func<vector,double> rosen = (v) => (1-v[0])*(1-v[0]) + 100*(v[1]-v[0]*v[0])*(v[1]-v[0]*v[0]);
		Func<vector,double> himmel = (v) => (v[0]*v[0]+v[1]-11)*(v[0]*v[0]+v[1]-11) + (v[0]+v[1]*v[1]-7)*(v[0]+v[1]*v[1]-7);
		Func<vector,double> sin = (v) => Math.Sin(v[0]);

		/* minimum should be [1,1] i think... */
		vector r_guess = new vector(3, 3);
		vector rosen_min = Minimization.QuasiNewton(rosen,r_guess);
		Console.WriteLine("\n --- Minimization of Rosenbrock's function --- ");
		Console.WriteLine($" Known minimum     = [1,1]");
		Console.WriteLine($" Starting position = [{r_guess[0]}, {r_guess[1]}]");
		Console.WriteLine($" Minimizer result  = [{rosen_min[0]}, {rosen_min[1]}]");
		Console.WriteLine($" Accuracy : [{rosen_min[0]-1}, {rosen_min[0]-1}]");		

		vector h_guess0 = new vector( 3.5, 2.5);
		vector h_guess1 = new vector(-2.5, 3.4);
		vector h_guess2 = new vector(-3.5,-3.0);
		vector h_guess3 = new vector( 3.2,-1.5);
		vector h_min0 = Minimization.QuasiNewton(himmel,h_guess0);
		vector h_min1 = Minimization.QuasiNewton(himmel,h_guess1);
		vector h_min2 = Minimization.QuasiNewton(himmel,h_guess2);
		vector h_min3 = Minimization.QuasiNewton(himmel,h_guess3);
		Console.WriteLine("\n --- Minimization of Himmelblau function --- ");
		Console.WriteLine($" Known minimum       = [3, 2]");
		Console.WriteLine($" starting position   = [{h_guess0[0]}, {h_guess0[1]}]");
		Console.WriteLine($" Minimizer result    = [{h_min0[0]}, {h_min0[1]}]\n");

		Console.WriteLine($" Known minimum       = [-2.805118, 3.131312]");
		Console.WriteLine($" starting position   = [{h_guess1[0]}, {h_guess1[1]}]");
		Console.WriteLine($" Minimizer result    = [{h_min1[0]}, {h_min1[1]}]\n");

		Console.WriteLine($" Known minimum       = [-3.779310, -3.283186]");
		Console.WriteLine($" starting position   = [{h_guess2[0]}, {h_guess2[1]}]");
		Console.WriteLine($" Minimizer result    = [{h_min2[0]}, {h_min2[1]}]\n");

		Console.WriteLine($" Known minimum       = [3.584428, -1.848126]");
		Console.WriteLine($" starting position   = [{h_guess3[0]}, {h_guess3[1]}]");
		Console.WriteLine($" Minimizer result    = [{h_min3[0]}, {h_min3[1]}]");
		
		vector sin_guess = new vector(1);
		sin_guess[0] = 3*(Math.PI)/2 * 0.9;
		vector sin_min = Minimization.QuasiNewton(sin,sin_guess);
		Console.WriteLine("\n --- Minimization of Sin function --- ");
		Console.WriteLine($" Known mimnimum   = {Math.PI*3/2} ~ pi*3/2");
		Console.WriteLine($" Minimizer result = {sin_min[0]}");
		Console.WriteLine($" accuracy is {sin_min[0] - 3*(Math.PI)/2}");

		Console.WriteLine("\n NOTE: number of steps can be seen in logA.tbc.txt \n");

		return 0;
	}
}
