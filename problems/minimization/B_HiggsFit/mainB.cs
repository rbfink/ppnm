using System;
using System.Collections.Generic;

public static class main
{
	static int Main()
	{
		List<double[]> Hdata = DataReader.ReadFile2List("higgsdata.txt");
		double[] E = Hdata[0];
		double[] sigma = Hdata[1];
		double[] error = Hdata[2];

		Func<vector, double> F = delegate(vector x)
		{
			double val = 0.0;
			for (int i=0; i<E.Length; i++)
			{
				val += (BW(E[i],x[0],x[1],x[2])-sigma[i])*(BW(E[i],x[0],x[1],x[2])-sigma[i]);
			}
			return val;
		};

		vector xguess = new vector(127.0, 4.0, 15.0);
		vector higgsparams = Minimization.QuasiNewton(F, xguess);

		var Writer = new System.IO.StreamWriter("ResultB.tbc.txt");
		Writer.WriteLine("\n --- Breit Wigner fit results ---\n");
		Writer.WriteLine($" m     = {higgsparams[0]}");
		Writer.WriteLine($" gamma = {higgsparams[1]}");
		Writer.WriteLine($" A     = {higgsparams[2]}\n");
		Writer.Close();

		var PlotDump = new System.IO.StreamWriter("fitdata.tbc.txt");
		double Emin = E[0];
		double Emax = E[E.Length-1];
		double dE = 0.01;
		for (double Eplot=Emin; Eplot<Emax; Eplot+=dE)
		{
			PlotDump.WriteLine("{0} {1}",Eplot, BW(Eplot, higgsparams[0], higgsparams[1], higgsparams[2]));
		}
		PlotDump.Close();
		return 0;
	}

	public static double BW(double E, double m, double gamma, double A)
	{
		return A/((E-m)*(E-m)+(gamma*gamma)/4);
	}
}
