using System;
using static System.Math;
using static System.Console;

public static class Minimization
{
	public static vector QuasiNewton
	(
		Func<vector,double> f,
		vector xstart,
		double eps=1e-4,
		int maxsteps=1000
	)
	{
		System.IO.TextWriter stderr = System.Console.Error; /* log writer */
		
		/* Variables and containers */
		int dim              = xstart.size; /* number of "coordinates" */
		matrix B             = new matrix(dim,dim); B.set_identity(); /* approximated Hessian */
		matrix dB            = new matrix(dim,dim);
		vector xs            = xstart.copy();
		vector y, xstep, df  = new vector(dim); /* containers */
		double dx            = 1e-7; /* smallest allowed stepsize */
		double alpha         = 1e-4; /* constant used in line backtracking */
		double lambda_min    = 1.0/64;
		int steps            = 0;
		double uty, lambda;

		while(steps < maxsteps)
		{
			steps++;
			/* Aproximate the function deriviative */
			df = Gradient(f,xs,dx);
			/* Find Newton step */
			xstep = -B*df;
			/* backtracking line search */
			lambda = 1.0;
			while( (f(xs+lambda*xstep) > f(xs)+alpha*(lambda*xstep).dot(df)) && (lambda > lambda_min) )
			{
				lambda = lambda/2;
			}
			xstep = lambda*xstep;
			/* check if any of the steps are too small */
			for (int i=0; i<dim; i++)
			{
				if(Abs(xstep[i]) < dx)
				{
					B.set_identity();
					break;
				}	
			}
			/* making new B matrix addition */
			vector dfps = Gradient(f, xs+xstep, dx);
			y = dfps - df;
			vector u = xstep-B*y;//(xstep-B*y).copy();
			uty = u.dot(y);
			/* acc. goal reached -> return coordinates */
			if (dfps.SimpleNorm() < eps)
			{
				stderr.WriteLine($" steps taken = {steps}");
				return xs+xstep;
			}
			/* reset inverse Hessian ie. B */
			else if (Abs(uty) < eps)
			{
				B.set_identity();
			}
			/* update hessian ie. B */
			else
			dB.set_zero();
			for (int i=0; i<dim; i++)
			{
				for (int j=0; j<dim; j++)
				{
					dB[i,j] = (u[i]*u[j])/uty;
				}
			}
			B = B + dB;
			xs += xstep;
		}// while

		/* This part is only reached if steps > maxsteps */
		stderr.WriteLine($"too many steps: step count > {steps}");
		return xs;

	}// QuasiNewton

	private static vector Gradient(Func<vector,double> f, vector x, double dx)
	{
		vector xdx = x.copy();
		vector gradvec = new vector(x.size);
		for (int i=0; i<x.size; i++)
		{
			xdx[i] += dx;
			gradvec[i] = (f(xdx)-f(x))/dx;
			xdx[i] = x[i];
		}
		return gradvec;
	}// Gradient
	
}// Minimization
