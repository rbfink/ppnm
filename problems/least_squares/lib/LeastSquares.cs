using System;
using static System.Math;
using static matrix;
using static MatrixManipulation;

public static class FittingTools
{
	public static Tuple<Func<double,int,double>,vector,matrix>LeastSquares(
		Func<double,double>[] fs
		,double[] xs
		,double[] ys
		,double[] dy)
	{
		// creating the A matrix and b vector for the system Ac=b
		int n = xs.Length;
		int m = fs.Length;
		matrix A = new matrix(n,m);
		vector b = new vector(n);
		for (int i=0; i<n; i++)
		{
			b[i] = ys[i]/dy[i];
			for (int j=0; j<m; j++)
			{
				A[i,j] = fs[j](xs[i])/dy[i];
			}
		}

		// Decomposing the A matrix : A=QR, remember A becomes Q !!!
		matrix R = new matrix(m,m);
		qr_gs_decomp(A,R);

		// Solving the overdetermined system
		vector c = qr_gs_solve(A, R, b);

		// calculation of the covariance matrix
		matrix Sigma = qr_gs_inverse(R.transpose()*R) ; // replacing the R matrix with it's inverse

		Func<double,int,double> fit_function = delegate(double x, int arg)
		{
			if (arg==0)
			{
				double fit = 0.0;
				for (int i=0; i<fs.Length; i++)
				{fit += c[i]*fs[i](x);}
				return fit;
			}
			else if (arg==1)
			{
				double sigmaplus_fit = 0.0;
				for (int i=0; i<fs.Length; i++)
				{sigmaplus_fit += (c[i]+Sqrt(Sigma[i,i]))*fs[i](x);}
				return sigmaplus_fit;
			}
			else if (arg==2)
			{
				double sigmaminus_fit = 0.0;
				for (int i=0; i<fs.Length; i++)
				{sigmaminus_fit += (c[i]-Sqrt(Sigma[i,i]))*fs[i](x);}
				return sigmaminus_fit;
			}
			else
			{
				System.Console.Error.WriteLine("\n Fit_function in LeastSquares : The integer argument must 0,1 og 2");
				return 0.0;
			}
		};		
		return new Tuple<Func<double,int,double>,vector,matrix>(fit_function, c, Sigma);
	}
}// LeastSquares
