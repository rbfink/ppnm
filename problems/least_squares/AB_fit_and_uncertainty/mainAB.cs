using System;
using static System.Math;
using static System.Console;
using static DataReader;

class main
{
	static int Main()
	{
		/* Reading tabulated data */
		double[][] data = ReadFile2da("../tabdata.txt");
		double[] datx = data[0];
		double[] daty = data[1];
		/* Generating the measuring error and logarithmic of assumed behavior */
		double[] y_err = new double[datx.Length];
		double[] logy  = new double[datx.Length];
		double[] dlogy = new double[datx.Length];
		for (int i=0; i<datx.Length; i++)
		{
			y_err[i] = daty[i]/20;
			logy[i]  = Log(daty[i]);
			dlogy[i] = y_err[i]/daty[i]; //daty[i]/y_err[i];
		}
		/*Since m=2, the included sum of functions only takes constant and linear function */
		Func<double,double>[] fs = new Func<double,double>[] {x => 1, x => x};
		
		Tuple<Func<double,int,double>,vector,matrix> results = FittingTools.LeastSquares(fs, datx, logy, dlogy);
		var Fit = results.Item1;
		vector opt_par = results.Item2;
		matrix Sigma = results.Item3;

		/* Printing fit */
		double start = datx[0];
		double slut  = datx[datx.Length-1];
		double dx    = 0.01;
		var datawriter = new System.IO.StreamWriter("data.tbc.txt");
		for (double plotx=start; plotx<slut; plotx+=dx)
		{
			datawriter.WriteLine($"{plotx} {Exp(Fit(plotx,0))}");
		}
		double hl    = -Log(2)/opt_par[1];

		double test  = Sqrt(Sigma[1,1]);
		double hlerr = Log(2)*misc.dabs(test/(opt_par[1]*opt_par[1])); // Sqrt(Sigma[1,1]);

		/* Writing log */
		WriteLine("\n --- Part A: Fit and half-life of ThX/224Ra ---\n");
		WriteLine($" The optimal parameters of A and lambda are : \n ln(a)  =  {opt_par [0]} \n lambda = {opt_par[1]} \n ");
		WriteLine(" The half-life of ThX, estimated from the fit is {0:f2} days", -Log(2)/opt_par[1]);
		WriteLine($" The half-life taken from wikipedia is 3.6319(23) days, so my calculation is an overestimation on the mean value.");
		WriteLine("\n --- Part B: Covariance matrix & half-life uncertainty ---\n");
		WriteLine($" Taking the uncertainty from the covariance matrix into account,");
		WriteLine(" the half-life with uncertainty becomes => {0:f2}({1:f2}) days (Seems a bit high?) \n",hl,hlerr);
		Sigma.print(" The covariance matrix: ");

		return 0;
	}// Main
}// main
