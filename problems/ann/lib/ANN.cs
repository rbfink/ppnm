using System;
using static System.Math;
using static System.Console;

/* making the ANN a class this time*/
public class ANN
{
	/* parameters */	
	public vector p; // container for neuron params 
	public Func<double,double> f; // neuron function
	public readonly int n; // number of neurons

	/* initializer */
	public ANN(int n, Func<double,double> f)
	{
		this.n = n;
		this.f = f;
		this.p = new vector(n*3);
	}// init

	/* feed forward link */
	public double feedforward(double x)
	{
		return feedforward(x, p);
	}

	/* feed forward */
	private double feedforward(double x, vector par)
	{
		double a, b, w;
		double sum = 0.0;
		for (int i=0; i<n; i++)
		{
			a = par[0+3*i];
			b = par[1+3*i];
			w = par[2+3*i];
			sum += f((x-a)/b)*w;
		}// for
		return sum;
	}// feedforward

	/* neuron trainer */
	public void train(vector xs, vector ys, double acc=1e-3, int limit=3000, bool log=false)
	{
		int calls = 0;
		/* Creating starting conditions for neurons */
		double xmin = xs[0];
		double xmax = xs[xs.size-1];
		double xstep = (xmax-xmin)/(n-1);
		for (int i=0; i<n; i++)
		{
			/* starting conditions taken from Simon Høgh */
			p[0+3*i] = xmin + i*xstep;
			p[1+3*i] = xstep;
			p[2+3*i] = 1.0;
		}// for

		Func<vector,double> delta = delegate(vector u)
		{
			calls++;
			p = u;
			double sum = 0.0;
			for (int i=0; i<xs.size; i++)
			{
				double dist = feedforward(xs[i]) - ys[i];
				sum += dist*dist;
			}
			return sum;
		};

		Minimization.QuasiNewton(delta, p, eps:acc, maxsteps:2000);
		Error.WriteLine($" training loops = {calls}");
	}// train
	
}// ANN
