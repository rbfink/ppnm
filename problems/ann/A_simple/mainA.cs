using System;
using static System.Math;
using static System.Console;

public static class main
{
	public static int Main()
	{
		Func<double,double> gwl = (x) => x*Exp(-x*x);

		int npoints = 15;
		double a=0, b=2*PI;
		vector xs = new vector(npoints);
		vector ys = new vector(npoints);
		for (int i=0;i<npoints;i++)
		{
			xs[i] = a + (b-a)/npoints * i;
			ys[i] = Cos(xs[i]);
		}//for

		int n = 5;
		var gwlnet = new ANN(n,gwl);
		gwlnet.train(xs, ys);

//		int printpoints = 50;
//		var rnd = new Random();
//		for (int i=0; i<printpoints; i++)
//		{
//			double x = b*rnd.NextDouble();
//			double y = Cos(x);
//			WriteLine("{0} {1}", x, y);
//		}

		for (int i=0; i<npoints; i++)
		{
			WriteLine($"{xs[i]} {ys[i]}");
		}

		var datadump = new System.IO.StreamWriter("fitA.tbc.txt");
		for (double z=a; z<b; z += 0.01)
		{
			datadump.WriteLine($"{z} {gwlnet.feedforward(z)}");
		}
		datadump.Close();

		return 0;
	}// Main
}// main
