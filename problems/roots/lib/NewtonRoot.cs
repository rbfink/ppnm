using System;
using static System.Math;
using static MatrixManipulation;

public static partial class Roots
{
	public static vector Newton
	(Func<vector,vector> f, vector x, double eps=1e-5, double dx=1e-7)
	{
		System.IO.TextWriter stderr = System.Console.Error;
		
		/* variables and containers */
		int dim = x.size;
		matrix J = new matrix(dim,dim);
		matrix R = new matrix(dim,dim);
		vector xs = x.copy();
//		vector xdx = x.copy();
		vector xstep = new vector(dim);
		int steps = 0;
		double lambda;
		double lambda_min = 1.0/64;

		/* calculation of root coordinates */		
		while( f(xs).norm() > eps && steps < 1000 )
		{
			steps++;
			/* generate J matrix */
			vector fx = f(xs);
			for (int i=0; i<dim; i++)
			{
				xs[i] += dx;
				vector df = f(xs)-fx;
				for (int j=0; j<dim; j++)
				{
					J[j,i] = df[j]/dx;
				}
				xs[i] -= dx;
			}
			/* solve linear system -> J*xstep = -f(x) */
			qr_gs_decomp(J, R);
			xstep = qr_gs_solve(J, R, -1*f(xs));
			/* optimize step */
			lambda = 1.0;
			while( f(xs+lambda*xstep).norm() > (1-lambda/2)*f(xs).norm()
				  && lambda > lambda_min )
			{
				lambda = lambda/2;
			}
			/* taking step */
			xs = xs + lambda*xstep;
		}// while
		stderr.WriteLine($" steps taken : {steps} ");
		return xs;
	}// Newton
	
}// Roots
