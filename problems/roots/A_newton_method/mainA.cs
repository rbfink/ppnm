using System;
using static System.Console;

public static class main
{
	static int Main()
	{
		Func<vector,vector> sin  = (x)
			=> new vector(Math.Sin(x[0]));

		Func<vector,vector> sin2 = (x)
			=> new vector(Math.Sin(x[0])*Math.Sin(x[0]));

		Func<vector,vector> quad = (x)
			=> new vector(-4*x[0]*x[0]*x[0] + 3*x[0]*x[0] + 25*x[0] +6);

		Func<vector,vector> rosen = (v)
			=> new vector(2*(v[0]-1) - 100*4*(v[1]*v[0] -v[0]*v[0]*v[0])
						 ,2*100*(v[1]-v[0]*v[0]));

		vector x_sin   = new vector(3.0);
		vector x_quad1 = new vector(-2.3);
		vector x_quad2 = new vector(-0.2);
		vector x_quad3 = new vector(2.0);
		vector v_rosen = new vector(0.8, 1.8);

		vector rootsin = Roots.Newton(sin,x_sin);
		vector root1 = Roots.Newton(sin2,x_sin);
		vector root2 = Roots.Newton(quad,x_quad1);
		vector root3 = Roots.Newton(quad,x_quad2);
		vector root4 = Roots.Newton(quad,x_quad3);
		vector root_rosen = Roots.Newton(rosen,v_rosen);

		WriteLine("\n --- Newtons root method simple tests ---\n");
		WriteLine(" Root of sin(x)");
		WriteLine(" root @ pi (starting x={0})   : result = {1,6:f3}, diff = {2,5:f6}",x_sin[0],rootsin[0],rootsin[0]-Math.PI);
		WriteLine("\n Root of sin(x)^2");
		WriteLine(" root @ pi (starting x={0})   : result = {1,6:f3}, diff = {2,5:f6}",x_sin[0],root1[0],root1[0]-Math.PI);
		WriteLine("\n Root of f(x) = -4x^3 + 3x^2 + 25x + 6");
		WriteLine(" root #1 @ -2    (starting x={0}) : result = {1,5:f3}, diff = {2,5:f6}",x_quad1[0],root2[0],root2[0]+2);
		WriteLine(" root #2 @ -0.25 (starting x={0}) : result = {1,5:f3}, diff = {2,5:f6}",x_quad2[0],root3[0],root3[0]+0.25);
		WriteLine(" root #3 @  3    (starting x= {0})   : result = {1,6:f3}, diff = {2,5:f6}",x_quad3[0],root4[0],root4[0]-3);

		WriteLine("\n --- Newtons root method: Rosenbrock's valley function  ---\n");
		WriteLine(" Root of d/di (1-x)^2 + 100(y-x^2)^2");
		WriteLine(" root @ [1,1] (starting [x,y]=[{0},{1}]) : result = [{2,6:f3},{3,6:f3}], diff = [{4,5:f6},{5,5:f6}]",v_rosen[0],v_rosen[1],root_rosen[0],root_rosen[1],root_rosen[0]-1,root_rosen[1]-1);
		WriteLine("\n notes: number of steps taken by the root finder can be found in logA.tbc.txt");
		WriteLine("");
		return 0;
	}
}
