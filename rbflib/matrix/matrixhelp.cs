using System;

public static partial class matrixhelp
{

	public static vector unitvector(int size, int entrance)
	{
		vector e_i = new vector(size);
		e_i[entrance] = 1;
		return e_i;
	}

	public static vector RandomVector(int n)
	{
		// generating random vector
		var V_rng = new Random();
		vector rv = new vector(n);
		for (int i=0; i<n; i++)
		{
			rv[i] = V_rng.NextDouble()*10;
		}
		return rv;
	}// RandomVector

	public static matrix RandomMatrix(int n, int m)
	{
		// generating random matrix
		var M_rng = new System.Random();
		matrix A = new matrix(n,m);
		for (int row=0; row<n; row++)
		{
			for (int col=0; col<m; col++)
			{
				A[row,col] = M_rng.NextDouble()*10;
			}
		}
		return A;
	}// RandomMatrix

	public static matrix RandomSymMatrix(int n, int eqdiag=0)
	{
		// generating random diagonal matrix
		var M_rng = new System.Random();
		matrix A = new matrix(n,n);
		double diag = M_rng.NextDouble()*10;
		for (int row=0; row<n; row++)
		{
			// diagonal elements
			if (eqdiag==0)
			{
				A[row,row] = M_rng.NextDouble()*10; /* random diagonal elements */
			}
			else if (eqdiag==1)
			{
				A[row,row] = diag;
			}
			for (int col=row+1; col<n; col++)
			{
				// off-diagonal elements
				A[row,col] = M_rng.NextDouble()*10;
				A[col,row] = A[row,col];
			}// for col
		}// for row
		return A;
	}// RandomSymMatrix

	 public static matrix BoxHamilton(int dimension)
	{
		/* generating particle in a box hamiltonian
		   Example for dimension = 4
				|-2  1  0  0|
			H =	| 1 -2  1  0|
				| 0  1 -2  1|
				| 0  0  1 -2|
		*/
		int n=dimension;
		double s=1.0/(n+1);
		matrix H = new matrix(n,n);
		for(int i=0;i<n-1;i++)
		{
		  matrix.set(H,i,i,-2);
		  matrix.set(H,i,i+1,1);
		  matrix.set(H,i+1,i,1);
		}
		matrix.set(H,n-1,n-1,-2);
		H *= -1/s/s;
		
		return H;
	}// BoxHamilton



}// matrixhelp
