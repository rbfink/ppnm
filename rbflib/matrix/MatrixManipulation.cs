using static System.Math;
using static matrix;
using static vector;

public static partial class MatrixManipulation
{

	public static void qr_gs_decomp(matrix A, matrix R)
	{// Takes a n*m matrix A and transforms it to a orthogonal matrix Q times
	 // an upper triangular matrix R.
	 //
	 // The matrix A is replaced with Q. Both matrices are returned...?

		if (R.size1!=A.size2 && R.size2!=A.size2)
		{
			throw new System.ArgumentException("The R matrix is not the right size or not square!");
		}
		int n = A.size1;
		int m = A.size2;
		matrix Q = A.copy();
		Q=A;
		for (int i=0; i<m; i++)
		{
			R[i,i] = Sqrt(innerproduct(A,i,A,i));
			for (int row=0; row<n; row++)
			{
				Q[row,i] = A[row,i]/R[i,i];
			}
			for (int j=i+1; j<m; j++)
			{
				R[i,j] = innerproduct(Q,i,A,j);
				for (int row=0; row<n; row++)
				{
					A[row,j] = A[row,j] - Q[row,i]*R[i,j];
				}// for row
			}// for j
		}// for i
	}// qr_gs_decomp

	public static vector qr_gs_solve(matrix Q, matrix R, vector b)
	{
		if (R.size1!=Q.size2 && R.size2!=Q.size2 && b.size!=Q.size1)
		{
	 		throw new System.ArgumentException("Dimensions of matrices ar vector not correct!!!");
	 	} 
		// system looks like this Ry=Q^(T)b = b'
		// Solving the linear system using backsubstitution
		vector x = Q.transpose()*b;
		int n = x.size;
		for (int i=n-1; i>=0; i--)
		{
			// calculating sum_{k=i+1}^{n}(U_ik*x_k)
			double sum_i = 0.0;
			for (int k=i+1; k<=n-1; k++)
			{
				sum_i += R[i,k]*x[k];
			}
			// putting it all together...
			x[i] = 1.0/R[i,i] * (x[i]-sum_i);
		}// for i
		return x;
	}// qr_gs_solve

	public static matrix qr_gs_inverse(matrix A)
	{
		if (A.size1 != A.size2)
		{
			throw new System.ArgumentException("This method only works for square matrices!");
		}
		
		int dim = A.size1;
		matrix Q = A.copy();
		matrix R = new matrix(dim,dim);
		matrix inverse = new matrix(dim,dim);
		qr_gs_decomp(Q,R);
		for (int col=0; col<dim; col++)
		{
			vector ei = unitvector(dim, col);
			vector xi = qr_gs_solve(Q, R, ei);
			for (int row=0; row<dim; row++)
			{
				inverse[row, col] = xi[row];
			}
		}		
		return inverse;
	}// qr_gs_inverse

	private static vector unitvector(int size, int entrance)
	{
		vector e_i = new vector(size);
		e_i[entrance] = 1;
		return e_i;
	}// unitvector

	private static double innerproduct(matrix A, int ai, matrix B, int bi)
	{
		if (A.size1 != B.size1){
			throw new System.ArgumentException("The two matrices do not have the same number of rows.");
		}
		double product = 0.0;
		for (int i=0; i<A.size1; i++){
			product += A[i,ai]*B[i,bi];
		}
		return product;
	}// innerproduct
}// MatrixManipulation
