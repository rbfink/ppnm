using System.Collections.Generic;

public class DataReader
{
	public static List<double[]> ReadFile2List(string filename)
	{
		/* Method stolen from Marco Majland*/
		string[] lines = System.IO.File.ReadAllLines(filename);
		int n = (lines[0].Split(' ')).Length;
		List<double[]> data = new List<double[]>(n);
		string[] subline;
		for(int i=0;i<n;i++)
		{
			data.Add(new double[lines.Length]);
		}
		for(int i=0;i<lines.Length;i++)
		{
			subline = lines[i].Split(' ');
			for(int j=0;j<n;j++){data[j][i] = double.Parse(subline[j]);}
		}
		return data;
	}//ReadFile2List

	public static double[][] ReadFile2da(string filename)
	{
		System.IO.TextWriter stderr = System.Console.Error;
		System.IO.StreamReader input = new System.IO.StreamReader(filename);
		// asserting data size
		int datasize = 0;
		while (input.ReadLine() != null) {datasize++;}
		// resetting reader
		input.Close();
		input = new System.IO.StreamReader(filename);
		// declaring data containers
		double[][] datareturn = new double[][]
		{
			new double[datasize],
			new double[datasize]	
		};
		//read and write data
		int linenumber = 0;
		do{
			string data = input.ReadLine(); //reads each line from the file
			if (data==null){break;}
			string[] datapoints = data.Split(' ',',','\t');
			if (datapoints.Length!=2)
			{
				// error handling. returns emtpy jagged array
				stderr.WriteLine(" !!! ReadFile2da error !!! too many or too few data columns, this method only transcribes 2D data. (i.e. x and y)");
//				double[][] error = //new double[][]
//				{
//					new double[]{0},
//					new double[]{0}
//				};
//				return error;
			}
			datareturn[0][linenumber] = double.Parse(datapoints[0]);
			datareturn[1][linenumber] = double.Parse(datapoints[1]);
			linenumber++;
		}while (true);
		return datareturn;
	}//ReadFile2da
}//DataReader
