public static class misc
{
	public static int dsgn(double x) /* sign function for doubles */
	{
		if (x >= 0)
		{return 1;}
		else
		{return -1;}
	}// dsgn

	public static double dabs(double x) /* abs function for doubles */
	{
		return dsgn(x)*x;
	}// dabs

	public static int BinarySearch(double[] xs, double target)
	{/*-------------------------------------------------------------------------
	    Binary search of ordered double[]: xs = [min,...........,max]
	    Returns index of the leftmost index relative to target, however if
	    target==xs[i] then the i-1 index is returned (except in the left edge case)
	 -------------------------------------------------------------------------*/
		int i = 0;
		int j = xs.Length-1;
		while (j-i>1)
		{
			int m = (i+j)/2;
			if (target -xs[m]>0)
			{i = m;}
			else
			{j=m;}
		}//while
		return i;
	}//BinarySearch
	
}// misc
