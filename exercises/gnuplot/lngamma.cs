using static System.Math;

public static partial class math{
	public static double lngamma(double x){
		double result = Log(gamma(x));
		return result;
	}
}