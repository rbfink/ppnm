using static System.Console;

static class main{
	static int Main(){
		for(double x=-3; x<=3; x+=0.25){
			WriteLine("{0,8:f3} {1,16:f8}", x, math.erf(x));
		}
		return 0;
	}
}