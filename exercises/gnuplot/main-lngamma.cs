using static System.Console;

static class main{
	static int Main(){
		double dx = 1.0/16;
		for(double x=0; x<=5; x+=dx){
			WriteLine("{0,10:f3} {1,15:f8}", x, math.lngamma(x));
		}//for
		return 0;
	}//Main
}//main