using static System.Console; // bruges til Error.Writeline og Writeline (stdin)
using static System.Math;

public static class main{
	public static int Main(){
		double eps = 1.0/32;
		double dx  = 1.0/16;
		for(double x = -4+eps; x<=6; x+=dx){
			WriteLine("{0,10:f3} {1,15:f8}", x, math.gamma(x)); //stdin
		}//for
		double truegamma = 1;
		for(double x=1; x<=100; x+=1){
			// Writing to the error stream being sent into err.txt by Makefile
			Error.WriteLine("{0,10:f3} {1:g15} {2:g15}", x, math.gamma(x), truegamma);
			truegamma *= x;
		}//for
		return 0;
	}//Main
}//class