using static System.Console;

class main{
	string s="old string \n";
	//System.Console.Write(s); # Man kan ikke kalde metoder
	// main classen, kun i Main metoden som er "entry point"
	// for programmet, og som gør det til en .exe fil
	static double multiply(double a,double b){
		//int i=1;
		return a*b;
	}

	static int Main(){
		// Primitive types in C#
		double x = 1.23;
		int nmax = 100;
		string s = "new string: hello æøå \n";
		System.Console.Write(s);
		// loops and booleans
		int one = 1;
		int two = 2;
		if(two>one){Write("2>1 \n");}
		else{Write("2!>1 \n");}

		int i=0; // variablen i bliver defineret som 0
		for(i=0;i<10;i++){Write("for loop: i={0} \n",i);}
		// nu er i=9 efter forloopet

		i=0; // variablen i bliver nu sat til 0
		while(i<10){Write("while loop: i={0} \n",i);i++;}

		do {Write("at least once \n");} while (false);
		while (false) {Write("never get here \n");}

	return 0;
	}
}
