using System;

static class stdinouterr
{
	public static int Main()
	{
		System.IO.TextReader stdin  = Console.In; //defines "stdin" as the IO text reader that can read from the console input stdin = System.Console.In
		System.IO.TextWriter stdout = Console.Out; //defines "stdout" as the IO text writer that writes to std output 
		System.IO.TextWriter stderr = Console.Error; //defines "stderr" as the IO text writer that writes to std error output
		stdout.WriteLine("# x, sin(x), cos(x)"); //skriver til std output stream
		stderr.WriteLine("# x, sin(x), cos(x)"); //skriver til std error stream
		do  // et "do" loop udfører indmaden indtil den angivne bool er "false", man kan bryde do looped med "break"
		{
			string s = stdin.ReadLine(); // læs fra std input
			if(s==null) break; // hvis der ikke er mere at læse, så stop do loopet
			string[] words = s.Split(' ',',','\t'); // the array "words" is filled with the characters spaced by ' '(space)
												    // , ','(comma) and '\t'(horisontal tabular). fx "23 2,4" becomes ["23","2","4"]
			foreach(var word in words){ 
				double x = double.Parse(word);
				stdout.WriteLine("{0}, {1}, {2}", x, Math.Sin(x), Math.Cos(x));
				stderr.WriteLine("{0}, {1}, {2}", x, Math.Sin(x), Math.Cos(x));
			}//foreach
		}while(true);//do
	return 0;	
	}//Main
}//class
