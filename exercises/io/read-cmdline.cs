using System;

class cmdline
{
	public static int Main(string[] args) // Main metode tager en liste med strenge som argumenter
	{
		Console.WriteLine("# x, sin(x), cos(x)"); // Hvad er det vi printer...
		foreach(var s in args)
		{ //"foreach" itererer over alle indgange i en instans som er "Enumerable"
			double x = double.Parse(s); //tager streng -> double fra argumentet givet i kaldet
			Console.WriteLine("{0}, {1}, {2}", x, Math.Sin(x), Math.Cos(x)); // udskriver argumentet som nu er en double
		}
		return 0;
	}//Main
}//cmdline
