public struct complex{
	private double re, im;
	double Re{get{return re};set;}
	double Im{get{return im};set;}
	public complex(double x, double y){re=x;im=y;}
	public static implicit operator complex(double x){return new complex(x);}

	public complex operator+(complex a, complex b){
		double x=a.Re+b.Re;
		double y=a.Im+b.Im;
		return new complex(x,y);
	}

	public complex operator-(complex a, complex b){
		double x=a.Re-b.Re;
		double y=a.Im-b.Im;
		return new complex(x,y);
	}

}
