class main{
static int Main(){

	int n=5; //Definerer størrelsen af vektoren
	vector v = new vector(n); //laver ny instans af objektet "vector"
	vector u = new vector(n); //laver ny instans af objektet "vector"
	//Her indsætter vi værdier ind i indgangende i vektorene	
	for(int i=0 ; i<n ; i++){
		v[i] = i;
		u[i] = 2*i;
	}
	v.print("v = "); //Her kalder vi print metoden i vector klassen
	u.print("u = ");
	vector w = u+v;
	w.print("w = ");
	(v*2).print("2*v = ");

	return 0;
	}
}
