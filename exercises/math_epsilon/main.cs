using System;
using static System.Console;
using static System.Math;
using static approx_method;

class main{
	static int Main(){
	//Exercise 1 (math)
	Write("-- Excercise 1 (math) -- \n");
	double x=1;
	Write("sin({0}) = {1} \n",x,Sin(x));
	Write("sqrt(2) = {0} \n",Sqrt(2));
	Write("Eulers number to the power of i = {0} \n",cmath.pow(E,complex.I));
	Write("exp(I*pi) = {0} \n",cmath.pow(E,complex.I*PI));
	Write("i^i = {0} \n",cmath.pow(complex.I,complex.I));
	Write("sin(i*pi) = {0} \n",cmath.sin(complex.I*PI));
	//Exercise 2 (epsilon)
	Write("-- Excercise 2 (machine epsilon) -- \n");
	//(A) min/max integer
	int i=1;
	while(i+1>i){i++;}
	Write("my max integer = {0} (int.MaxValue = {1}) \n",i,int.MaxValue);
	i=0;
	while(i-1<i){i--;}
	Write("my min integer = {0} (int.MinValue = {1}) \n",i,int.MinValue);
	//(B) "Machine epsilon"
	double xd=1.0; //for double
	while(1+xd!=1){xd/=2;} // så længe C# ikke kan se forskel på 1+x halver x og prøv igen
	xd*=2; // dette er tallet hvor C# KAN se forskel
	Write("machine epsilon for double = {0} (System.Math.Pow(2,-52)={1}) \n"
		,xd,System.Math.Pow(2,-52));
	float xf=1f;
	while(1f+xf!=1f){xf/=2f;}
	xf*=2f;
	Write("machine epsilon for float = {0} (System.Math.Pow(2,-23)={1}) \n"
		,xf,System.Math.Pow(2f,-23f));
	//(C) Harmonic sum
	int max = int.MaxValue/2;
	float float_sum_up = 1f;
	int n=1;
	for(n=1; n<max+1; n++){// sum from 1 and up
		float_sum_up = float_sum_up + float_sum_up/n;
	}
	Write("fharmonic sum up   = {0} \n",float_sum_up);
	
	float float_sum_down = 1f/max;
	for(n=max-1; n>0; n--){// sum from 1f/max down to 1 
		float_sum_down = float_sum_down + 1f/n;
	}
	Write("fharmonic sum down = {0} \n", float_sum_down);
	
	// Same calculation, but for doubles instead
	double double_sum_up = 1.0;
	for(n=1; n<max+1; n++){// sum from 1 and up
		double_sum_up = double_sum_up + double_sum_up/n;
	}
	Write("dharmonic sum up   = {0} \n", double_sum_up);
	
	double double_sum_down = 1.0/max;
	for(n=max-1; n>0; n--){// sum from 1f/max down to 1 
		double_sum_down = double_sum_down + 1.0/n;
	}
	Write("dharmonic sum down = {0} \n", double_sum_down);

	double test = 1;
	Write("approx true .. -> {0} \n", approx(test,test+1e-10));

	return 0;
	}
}






