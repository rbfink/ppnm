using System;
using static System.Math;
using static System.Console;

class main
{
	static int Main()
	{
		double ll; // lower limit
		double ul; // upper limit
		double[] results = new double[6]; // list to be filled wíth results

		double p = 5.5;
		
		// A.1
		Func<double,double> ln_sqrt = (x) => Log(x)/Sqrt(x);
		// A.2
		Func<double,double> exp_x2  = (x) => Exp(-x*x);
		// A.3
		Func<double,double> ln_1ox  = (x) => Pow(Log(1/x),p);

		ll = 0;
		ul = 1;
		results[0] = quad.o8av(ln_sqrt, ll, ul);
		results[2] = quad.o8av(ln_1ox, ll, ul);
		
		ll = double.PositiveInfinity;
		ul = double.NegativeInfinity;
		results[1] = quad.o8av(exp_x2, ll, ul);		

		WriteLine("--- opg. A ---");
		WriteLine("int_0^1(ln(x)/sqrt(x)) = {0}", results[0]);
		WriteLine($"should be -------------> {-4}");
		WriteLine();	
		WriteLine("int_-inf^inf(-exp(x²)) = {0}", results[1]);
		WriteLine($"should be -------------> {Sqrt(PI)}");
		WriteLine();
		WriteLine("ln(1/x)^p                = {0}", results[2]);
		WriteLine($"should be -------------> {math.gamma(p+1)}");

		ll = 0.0;
		ul = double.PositiveInfinity;
		// B.1
		Func<double,double> Bernoulli = (x) => (x)/(Exp(x)-1);
		results[3] = quad.o8av(Bernoulli, ll, ul);
		// B.2
		Func<double,double> xxx = (x) => (x*x*x)/(Exp(x)-1);
		results[4] = quad.o8av(xxx, ll, ul);
		WriteLine("\n--- opg. B ---");
		WriteLine("int_0^inf(x*x/(Exp(x)-1)) = {0}", results[3]);
		WriteLine($"should be -------------> {PI*PI/6}");
		WriteLine();	
		WriteLine("int_0^inf(x*x*x/(Exp(x)-1)) = {0}", results[4]);
		WriteLine($"should be -------------> {PI*PI*PI*PI/15}");
		WriteLine();

		ll = double.NegativeInfinity;
		ul = double.PositiveInfinity;
		// C.a
		Func<double,Func<double, double>> Gauss = (y) => {return (x) => Exp(-y * x*x);};
		Func<double,Func<double, double>> Hamilton = (y) => {return (x) => ((-y*y * x*x + y/2 + x*x/2) * Exp(-y * x*x));};

		for (double i = 0.1; i<=3; i+=0.1)
		{
			double result = quad.o8av(Hamilton(i), ll, ul)/quad.o8av(Gauss(i), ll, ul);
			Error.WriteLine($"{i} {result}");
		}
		return 0;
	}//Main
}//main
