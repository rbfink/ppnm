public struct vector3d{

	// private attributer som kan get'es og set'es gennem x,y,z
	private double _x;
	private double _y;
	private double _z;
	// reel metode som returner de private værdier som kan ændres gennem et sikkerheds proxy led i form af "set"
	public double x {get{return _x;} set{_x = value;}}
	public double y {get{return _y;} set{_y = value;}}
	public double z {get{return _z;} set{_z = value;}}
	
	//constructor
	public vector3d(double a, double b, double c){
		_x = a;
		_y = b;
		_z = c;
	}
	
	//Overloading ToString;
	public static string ToString(vector3d v){
		return string.Format("[{0}, {1}, {2}]", v.x, v.y, v.z);
	}

	//Overloading operators
	public static vector3d operator+(vector3d v, vector3d u){
		return new vector3d(v.x + u.x, v.y + u.y, v.z + u.z);
	}
	
	public static vector3d operator-(vector3d v, vector3d u){
		return new vector3d(v.x - u.x, v.y - u.y, v.z - u.z);
	}
	
	public static vector3d operator*(vector3d v, double a){
		return new vector3d(a*v.x, a*v.y, a*v.z);
	}
	
	public static vector3d operator*(double a, vector3d v){
		return v*a;
	}

	public static vector3d operator/(vector3d v, double a){
		return new vector3d(v.x/a, v.y/a, v.z/a);
	}
	//// Methods
	// dot product
	public static double dot_product(vector3d v, vector3d u){
		double r = v.x*u.x + v.y*u.y + v.z*u.z;
		return r;
	}

	// cross product
	public static vector3d cross_product(vector3d v, vector3d u){
		return new vector3d(v.y*u.z - v.z*u.y
						   ,v.z*u.x - v.x*u.z
						   ,v.x*u.y - v.y*u.x);
	}

} //struct