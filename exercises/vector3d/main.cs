	using static System.Console;

static class main{
	static int Main(){

		vector3d a = new vector3d(0, 1, 0);
		vector3d b = new vector3d(1, 0, 0);
		int tint  = 2;

		WriteLine($"a = {vector3d.ToString(a)}");
		WriteLine($"b = {vector3d.ToString(b)}");
		
		// test af vector3d.ToString metodeń
		WriteLine();
		WriteLine("forventet output af ToString -> [{0}, {1}, {2}]"
		,a.x, a.y, a.z); 
		WriteLine($"output af ToString -----------> {vector3d.ToString(a)}");
		WriteLine();
		
		// test af + - * og /
		WriteLine($"a+b    = { vector3d.ToString(a+b)    }");
		WriteLine($"a*tint = { vector3d.ToString(a*tint) }");
		WriteLine($"a/tint = { vector3d.ToString(a/tint) }");
		WriteLine();

		// test af dot_product og cross_product
		WriteLine($"forventet output af dot_product(a,b) = {a.x*b.x + a.y*b.y+a.z*b.z}");
		WriteLine($"a dot b                              = {vector3d.dot_product(a,b)}");
		WriteLine($"a x b = { vector3d.ToString( vector3d.cross_product(a,b) ) }");

		return 0;
	}//Main
}//main