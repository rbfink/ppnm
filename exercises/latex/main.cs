using System;
using System.Collections.Generic;

class main
{
	static int Main(string[] args)
	{
		double[] input = new double[args.Length];
		for(int i=0; i<args.Length; i++)
		{
			input[i] = double.Parse(args[i]);
		}
		vector init = new vector(input[0], input[0]);
		double a = input[1];
		double start = input[2];
		double slut = input[3];
		Tuple<double[],double[]> XandY = libexp.ExpSolver(init
														 ,start
														 ,slut
														 ,a);
		// printing date
		for(int i=0; i<XandY.Item1.Length; i++)
		{
			Console.WriteLine($"{XandY.Item1[i]} {XandY.Item2[i]}");
		}
		return 0;
	}//Main
}//main