using System;
using static System.Math;
using System.Collections.Generic;

public static class libexp
{
	public static Tuple<double[],double[]> ExpSolver(vector init
													,double start
													,double slut
													,double a)
	{
		Func<double,vector,vector> expfunc = delegate(double x, vector y)
				{
					return new vector(a*y[0], a*a*y[0]);
				};
		List<double> xs = new List<double>();
		List<vector> ys = new List<vector>();
		ode.rk23(expfunc, start, init, slut, xlist:xs, ylist:ys);
		double[] x_return = new double[xs.Count];
		double[] y_return = new double[xs.Count];
		for(int i=0; i<xs.Count; i++)
		{
			x_return[i] = xs[i];
			y_return[i] = ys[i][0];
		}
		return Tuple.Create<double[],double[]>(x_return, y_return);
	}//ExpSolver
	
	public static Func<double,double,double> ExpTest = delegate(double x, double a)
	{
		return Exp(a*x);	
	};
}// class