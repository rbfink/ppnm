using System;
using System.Collections.Generic;

public class opgB
{
	public static int OrbitSolver(vector init, double start, double slut, double e)
	{
		Func<double,vector,vector> eq_orbit = delegate(double x, vector y)
			{
				return new vector(y[1], 1-y[0]+e*y[0]*y[0]);
			};

		var xs = new List<double>();
		var ys = new List<vector>();

		ode.rk23(eq_orbit, start, init, slut, xlist:xs, ylist:ys, h:(2*Math.PI)/(128));

		for(int i=0; i<xs.Count; i++)
		{
			System.Console.WriteLine($"{xs[i]} {ys[i][0]}");
		}
	return 0;	
	} 
}