using System;
using System.Collections.Generic;
using static System.Console;

class main
{
	static int Main(string[] args)
	{
		System.IO.TextReader stdin = Console.In; // The main file will be run several times for the different calls in the excercise
		System.IO.TextWriter stderr = Console.Error;
		double[] inputs = new double[5];
		
		if(args.Length != 5)
		{
			stderr.WriteLine("!! Error message !! wrong number of inputs given. Five is needed: string<u du eps start slut>");
			return 1;
		}
		for(int i=0; i<args.Length; i++)
		{
			inputs[i] = double.Parse(args[i]);
		}
		vector v 	 = new vector(inputs[0], inputs[1]);
		double eps   = inputs[2]; // relativistic correction
		double begin = inputs[3];
		double end   = inputs[4];

		opgB.OrbitSolver(v, begin, end, eps);
		
		return 0;
	} //Main
} //main
