using System;
using static System.Math;

public static class opgA
{
	// function in the System namespace that depends on x and y
	public static Func<double,vector,vector> dF = delegate(double x, vector y)
	{
		return new vector(y[0]-y[0]*y[0], 0);
	};

	// logistic test function
	public static Func<double,double,double> LogisticF = delegate(double x, double x_0)
	{
		return 1/(1 + Exp(-(x-x_0)) );
	};
}