using System.Collections.Generic;
using static System.Console;

class main
{
	static int Main()
	{
		double start, slut; // start and end of integration/differentiation
		double y_0, dy_0;   // initial conditions
		vector v1 = new vector(2);
		// implicitly typed variables, ie. the the compiler determines the type (only works in a method scope like Main etc.)
		var xs = new List<double>();
		var ys = new List<vector>();

		start = 0;
		slut  = 3;
		y_0   = 0.5;
		dy_0  = 0.25;
		v1[0] = y_0;
		v1[1] = dy_0;

		ode.rk23(opgA.dF, start, v1, slut, xlist:xs, ylist:ys);

		// printing A-data
		for(int i=0; i<xs.Count; i++)
		{
			WriteLine($"{xs[i]} {ys[i][0]} {opgA.LogisticF(xs[i],0)}");
		}
		return 0;
	}//Main
}//main
